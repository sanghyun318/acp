/*
 * GiGA IoT Platform version 2.0
 *
 *  Copyright ⓒ 2015 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package com.kt.iotmakers.platform;

/**
 * Const
 * @author Sangsun Park (blue.park@kt.com)
 *
 */
public class Const {

	public static final String PACKAGE = "com.kt.iotmakers.platform";
	public static final String ACP_API_PATH = "/policies";
	public static final String RESC_API_PATH = "/resources";
	public static final String RESPONSE_CODE_OK		= "200";
	public static final String KEY_DELIMITER = "#";
	public static final String ACP_DELIMITER = ":";
	public static final String ARRAY_DELIMITER = ",";
	
	public static final String PREFIX_E2E_ID = "DEFAULT_E2E";
	
	public static final String KAFKA_OPERATION_TYPE = "kafkaOperationType";
	public static final String KAFKA_OPERATION_OWNER = "kafkaOperationOwner";

	public static final int DEFAULT_CONSUME_THREAD_COUNT = 3;
	public static final String  TOKEN_TYPE_ADMIN = "admin";
	public static final String  TOKEN_ADMIN = "myAdmin";
	public static final String  TOKEN_TYPE = "tokenType";
	public static final String  BEARER_PARSING = "Bearer ";
	
	public static final String  GRANTOR = "grantor";
	public static final String  OPERATION_TYPE = "operationType";
	public static final String  ORIGINATORS = "originators";
	public static final String  ORIGINATOR = "originator";
	public static final String  RESOURCE_URI_LIST = "resourceUriList";
	public static final String  RESOURCE_URI = "resourceUri";
	public static final String  MEMBER_SEQUENCE = "memberSequence";
	public static final String  EXTR_MEMBER_SEQUENCE = "extrMemberSeuquence";
	public static final String  GEN_ACP_ID_PREFIX = "acp";

	//context 관련
	public static final String  TIME_CONTEXT_TYPE = "time";
	public static final String  TIME_CONTEXT_EXPIRES_AT = "expiresAt";
	
	//acp 관련
	public static final String ACP_ID = "acpId";
	public static final String MASTER_ACP_ID_PREFIX = "acp:00:";
	public static final String GRANT_ACP_ID_PREFIX = "acp:01:";
	public static final String ACP_TYPE = "acpType";
	
	public static final String ACP_NAME_PREFIX = "ACP";
	public static final String ACP_OWNER_NAME = " for resource Onwer by ";
	public static final String ACP_GRANTEE_NAME = " for resource grantee to ";
//	public static final String ACP_TYPE = "acpType";
	
	//acr 관련
	public static final String ACR_TYPE = "acrType";
	public static final String ACR_TYPE_PV = "pv";
	public static final String ACR_TYPE_SPV = "spv";
}
