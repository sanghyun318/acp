package com.kt.iotmakers.platform.api.VO.Contract;

import lombok.Data;
import lombok.ToString;

@ToString
public @Data class Contract {

	public static final String indentifier = "Contract";
	
	String id;
	Long sequence;
	Long creator;
	
	public Contract(Long sequence) {
		super();
		this.sequence = sequence;
	}
	
	public Contract() {
		
	}

}
