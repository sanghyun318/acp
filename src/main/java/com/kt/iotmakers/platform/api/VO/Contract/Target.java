package com.kt.iotmakers.platform.api.VO.Contract;

public class Target {
	
	public Target() {
	}
	
	
	public Target(Long sequence) {
		this.sequence = sequence;
	}


	private Long sequence;
	private String districtCode;
	private String themeCode;
	private String serviceCode;
	private String creator;
	private String id;	
	private String delYn;

	public Long getSequence() {
		return sequence;
	}


	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}


	public String getDistrictCode() {
		return districtCode;
	}


	public void setDistrictCode(String districtCode) {
		this.districtCode = districtCode;
	}


	public String getThemeCode() {
		return themeCode;
	}


	public void setThemeCode(String themeCode) {
		this.themeCode = themeCode;
	}


	public String getServiceCode() {
		return serviceCode;
	}


	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}


	public String getCreator() {
		return creator;
	}


	public void setCreator(String creator) {
		this.creator = creator;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getDelYn() {
		return delYn;
	}


	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Target [sequence=" + sequence + ", districtCode=" + districtCode + ", themeCode=" + themeCode
				+ ", serviceCode=" + serviceCode + ", creator=" + creator + ", id=" + id + ", delYn=" + delYn + "]";
	}
	
	
}
