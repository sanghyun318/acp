package com.kt.iotmakers.platform.api.message.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.kt.iotmakers.platform.api.VO.Contract.Contract;
import com.kt.iotmakers.platform.api.VO.Device.Device;
import com.kt.iotmakers.platform.api.v2.accessControl.service.AccesscontrolService;
import com.kt.iotmakers.platform.base.util.ObjectConverter;
import com.kt.iotmakers.platform.common.Constant.OperationType;
import com.kt.iotmakers.platform.common.base.domain.event.MasterEvent;

import lombok.extern.slf4j.Slf4j;

@Profile("local")
@Slf4j
@Component
public class MasterComsumer {


	@Autowired
	private AccesscontrolService accesscontrolService;
	
	
	@EventListener(condition = "#event.topic == 'MASTER.TARGET'")
	public void targetEventListener(MasterEvent event) throws Exception {
		if(event == null || event.getBody() == null) {
			
			log.warn("event is null or event has no body {}", event!=null?event.toString():null);
			return;
		}
		
		log.debug(event.toString());
		log.debug("------receive targetEvent : {}", event);
		
		Contract contract = ObjectConverter.toObject((Map<String, Object>)event.getBody(), Contract.class); //java.util.LinkedHashMap event.getBody()
		OperationType operationType = OperationType.valueOf(event.getOperationType());
		
		log.debug("parsed target :{}, parsed operationType : {}", contract, operationType);
		handleMasterEvent(contract.getCreator(), convertToResourceUri(contract, Contract.class), operationType);
	}
	
	/**
	 * 
	 * @author : SanghyunLee
	 * @return : String
	 * @desc   : consuming 한 객체를 모듈이 관리하는 객체로 변경한다. 
	 * @param <T>
	 * @param object
	 * @param classType
	 * @return
	 */
	private <T> String convertToResourceUri(T object, Class<?> classType) {

		String className = classType.getSimpleName();
		log.info("className : {}", className);
		
		StringBuilder builder = new StringBuilder("/");
		
		switch (className) {
			case Contract.indentifier:
				
				log.debug("find contract in switch");
				builder.append("target/").append(((Contract)object).getSequence());
				return builder.toString();
				
			case Device.indentifier:
				log.debug("find Device in switch");
				
				builder.append("target/").append(getTargetKeyFromDevice((Device)object)).append("/device/").append(((Device)object).getSequence());
				
				return builder.toString();
			default :
			 log.debug("default in switch");
			 return null;
			 
			}
	}

	private Long getTargetKeyFromDevice(Device object) {
		if(object == null || object.getContract() == null) {
			log.warn("device is null or has no contract : {}", object);
			return null;
		}
		
		return object.getContract().getSequence();
	}

	private void handleMasterEvent(Long memberSequence, String resourceUri, OperationType operationType) {
		log.debug("handleMasterEvent : memberSequence {}, resourceUri {} ,operationTyppe {}", memberSequence, resourceUri, operationType);
		
		switch (operationType) {
			case CREATE:
				accesscontrolService.registerResource(memberSequence, resourceUri, null);
				break;
			case UPDATE:
				//resource가 update 되는 경우는 ????, name 변경??, 리소스 부가 정보 변경.. 이걸 알아야햘 필요가 있을까????
				//리소스의 주인이 바뀌는 것만 감지하면 됨
				accesscontrolService.updateResource(memberSequence, resourceUri, null);
				break;
			case DELETE:
				accesscontrolService.deleteResource(memberSequence, resourceUri, null);
				break;
			case RETRIEVE:
				log.debug("no logic for {}", operationType.toString());
				break;
			case REQUEST:
				log.debug("no logic for {}", operationType.toString());
				break;
			case RESPONSE:
				log.debug("no logic for {}", operationType.toString());
				break;
	
			default:
				log.debug("no logic for default");
				break;
			}

	}

	@EventListener(condition = "#event.topic == 'MASTER.DEVICE'")
	public void deviceEventListener(MasterEvent event) throws Exception {
		log.debug(event.toString());

		
		log.debug("------receive deviceEvent : {}", event);
	}

}
