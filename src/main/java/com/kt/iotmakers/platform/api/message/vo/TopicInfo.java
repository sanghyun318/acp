package com.kt.iotmakers.platform.api.message.vo;

public class TopicInfo {

	String name;
	String groupId;
	
	public TopicInfo() {
		
	}
	public TopicInfo(String name, String groupId){
		this.name = name;
		this.groupId = groupId;
	}

	/**
	 * @return the topic
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the groupId
	 */
	public String getGroupId() {
		return groupId;
	}

	/**
	 * @param name the topic to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TopicInfo [topic=" + name + ", groupId=" + groupId + "]";
	}
}
