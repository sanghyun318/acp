package com.kt.iotmakers.platform.api.v10.UtilApi.controller;
///**
// * <PRE>
// *  Project 3MP.master-api
// *  Package com.kt.iot.api.v11.member.controller
// * </PRE>
// * @brief
// * @file MemberController.java
// * @date 2015. 11. 4. 오후 3:49:03
// * @author kim.seokhun@kt.com
// *  변경이력
// *        이름     : 일자          : 근거자료   : 변경내용
// *       ------------------------------------
// *        kim.seokhun@kt.com  : 2015. 11. 4.       :            : 신규 개발.
// *
// * Copyright © 2013 kt corp. all rights reserved.
// */
//package com.kt.iot.api.v10.UtilApi.controller;
//
//import static  org.springframework.web.bind.annotation.RequestMethod.GET;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.kt.iot.Const;
//import com.kt.iot.Version;
//import com.kt.iot.api.message.service.AbstractMessageProduceService;
//import com.kt.iot.base.mvc.message.Messages;
//import com.kt.iot.config.PropertyConfig.KafkaInfoProperty;
//import com.kt.iot.config.properties.KafkaInfo;
//
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiParam;
//import springfox.documentation.annotations.ApiIgnore;
//
///**
// * <PRE>
// *  ClassName MemberController
// * </PRE>
// * @brief
// * @version 1.0
// * @date 2015. 11. 4. 오후 3:49:03
// * @author kim.seokhun@kt.com
// */
//@RestController
//@RequestMapping("/" + Version.V2+"/config")
//public class ConfigTest {
//
//	private static final Logger logger = LoggerFactory.getLogger(ConfigTest.class);
//	@Autowired
//	KafkaInfoProperty kafkaProperties;
//	@ApiOperation(value = "프로퍼티 테스트", notes = "프로퍼티 테스트.", httpMethod = "GET", response = String.class)
//    @RequestMapping(value="", method=GET)
//	private String getMemgers(
//			@ApiParam(name = "envVal", value = "", required = false) @RequestParam(value = "envVal",required = false) String findEnv,
//							 	 	@ApiIgnore
// 	 								Messages messages) {
//		
//		String envReslt = null;
//		
//		
//		KafkaInfo info = kafkaProperties.getKafkaInfo();
//		
//		
//		
//		if(findEnv != null)
//			envReslt = System.getenv(findEnv);
//
//	    logger.info("host:{}", System.getenv("dbHost"));
//	    logger.info("port:{}", System.getenv("dbPort"));
//	    logger.info("dbName:{}", System.getenv("dbName"));
//	    logger.info("dbUserName:{}", System.getenv("dbUserName"));
//	    logger.info("dbUserPassword:{}", System.getenv("dbUserPassword"));
//    	
//		messages.addMessage(Const.RESPONSE_CODE_OK, "");
//		return info.toString();
//	}
//	
//	
//	
//}
