/**
 * <PRE>
 *  Project 3MP.master-api
 *  Package com.kt.iot.api.v11.member.controller
 * </PRE>
 * @brief
 * @file MemberController.java
 * @date 2015. 11. 4. 오후 3:49:03
 * @author kim.seokhun@kt.com
 *  변경이력
 *        이름     : 일자          : 근거자료   : 변경내용
 *       ------------------------------------
 *        kim.seokhun@kt.com  : 2015. 11. 4.       :            : 신규 개발.
 *
 * Copyright © 2013 kt corp. all rights reserved.
 */
package com.kt.iotmakers.platform.api.v10.UtilApi.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kt.iotmakers.platform.Const;
import com.kt.iotmakers.platform.Version;
import com.kt.iotmakers.platform.api.v2.accessControl.vo.TimeContextFactor;
import com.kt.iotmakers.platform.base.mvc.message.Messages;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import springfox.documentation.annotations.ApiIgnore;

/**
 * <PRE>
 *  ClassName MemberController
 * </PRE>
 * @brief
 * @version 1.0
 * @date 2015. 11. 4. 오후 3:49:03
 * @author kim.seokhun@kt.com
 */
@RestController
@RequestMapping("/" + Version.V2+"/context")
public class ContextTest {

	private static final Logger logger = LoggerFactory.getLogger(ContextTest.class);
	 
	@ApiOperation(value = "context 테스트 API", notes = "context 테스트 API 조", httpMethod = "GET", response = String.class)
    @RequestMapping(value="", method=RequestMethod.GET)
	private void login(
			@ApiParam(name = "context", value = "", required = false) 
							 	 	@ApiIgnore
 	 								Messages messages) {
			
    	TimeContextFactor timeContextFactor = new TimeContextFactor();
    	timeContextFactor.setSequence(123123123L);
    	timeContextFactor.setType(Const.TIME_CONTEXT_TYPE);
    	timeContextFactor.setExpiredYn(true);
    	timeContextFactor.setExpiresAtAfter(999999L);
    	
    	logger.info("time context factor : {}",timeContextFactor);
		return ;
	}
	
	
	
}
