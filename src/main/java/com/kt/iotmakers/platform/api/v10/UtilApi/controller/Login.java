/**
 * <PRE>
 *  Project 3MP.master-api
 *  Package com.kt.iot.api.v11.member.controller
 * </PRE>
 * @brief
 * @file MemberController.java
 * @date 2015. 11. 4. 오후 3:49:03
 * @author kim.seokhun@kt.com
 *  변경이력
 *        이름     : 일자          : 근거자료   : 변경내용
 *       ------------------------------------
 *        kim.seokhun@kt.com  : 2015. 11. 4.       :            : 신규 개발.
 *
 * Copyright © 2013 kt corp. all rights reserved.
 */
package com.kt.iotmakers.platform.api.v10.UtilApi.controller;

import static  org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.HashMap;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kt.iotmakers.platform.Const;
import com.kt.iotmakers.platform.Version;
import com.kt.iotmakers.platform.api.VO.UserInfo;
import com.kt.iotmakers.platform.base.exception.BadRequestException;
import com.kt.iotmakers.platform.base.message.ResponseCode;
import com.kt.iotmakers.platform.base.mvc.message.Messages;
import com.kt.iotmakers.platform.base.util.ObjectConverter;

import io.jsonwebtoken.Jwts;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import springfox.documentation.annotations.ApiIgnore;

/**
 * <PRE>
 *  ClassName MemberController
 * </PRE>
 * @brief
 * @version 1.0
 * @date 2015. 11. 4. 오후 3:49:03
 * @author kim.seokhun@kt.com
 */
@RestController
@RequestMapping("/" + Version.V2+"/login")
public class Login {

	private static final Logger logger = LoggerFactory.getLogger(Login.class);
	 public static final HashMap<String, String> user = new HashMap<String, String>(){
		 {
			 put("iotUserA", "10010");
			 put("iotUserB", "10011");
			 put("iotUserC", "10012");
		 }

	 };

	@ApiOperation(value = "로그인 API", notes = "로그인 API", httpMethod = "POST", response = String.class)
    @RequestMapping(value="", method=POST)
	private String login(
			@ApiParam(name = "로그인 정보", value = "", required = false) @RequestBody(required = false) UserInfo userInfo,
							 	 	@ApiIgnore
 	 								Messages messages) {
			
    	logger.info("user into : {}",userInfo);
    	String id = userInfo.getId();
    	
    	String mbrSeq= null;
    	if(id != null && user.containsKey(id)){
    		mbrSeq = user.get(id);
    		
    	}else{
    		throw new BadRequestException(ResponseCode.INTERNAL_AUTHENTICATION_FAILED);
    	}
    	String token = Jwts.builder().claim("mbr_seq",mbrSeq).compact();
    	logger.info("token?? : {}",token);
    	
    	Jwt jwt = JwtHelper.decode(token);
		JSONObject jsonObject = null;
		try {
			jsonObject = ObjectConverter.toJSON(jwt.getClaims());
			logger.info("json token :{}",jsonObject.toJSONString());
		} catch (JsonProcessingException | ParseException e) {
			// TODO Auto-generated catch block
			logger.error(e.toString());
		}
    	
		messages.addMessage(Const.RESPONSE_CODE_OK, "");
		return token;
	}
	
	
	
}
