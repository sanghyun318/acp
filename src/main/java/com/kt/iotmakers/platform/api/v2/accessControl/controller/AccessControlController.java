package com.kt.iotmakers.platform.api.v2.accessControl.controller;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kt.iotmakers.platform.Const;
import com.kt.iotmakers.platform.Version;
import com.kt.iotmakers.platform.api.v2.accessControl.service.AccesscontrolService;
import com.kt.iotmakers.platform.api.v2.accessControl.vo.AccessControl;
import com.kt.iotmakers.platform.api.v2.accessControl.vo.AcpOperationType;
import com.kt.iotmakers.platform.base.exception.BadRequestException;
import com.kt.iotmakers.platform.base.message.ResponseCode;
import com.kt.iotmakers.platform.base.mvc.message.Messages;
import com.kt.iotmakers.platform.base.util.ResourceParsingUtil;
import com.kt.iotmakers.platform.config.properties.ResourceInfo;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import springfox.documentation.annotations.ApiIgnore;


@Slf4j
@RestController
@RequestMapping("/" + Version.V2 + Const.ACP_API_PATH)
public class AccessControlController {

	
	@Autowired
	AccesscontrolService accessConrtolService;
	@Autowired
	ResourceParsingUtil resourceParsingUtil;

	@ApiOperation(value = "접근 가능한 권한 정책 목록 조회", notes = "회원이 접근 가능한 권한 정착 목록을 조회합니다.", httpMethod = "GET")
    @GetMapping(value="")
	private List<AccessControl> retrieveAccessControlPolices(
									@ApiParam(name="Authorization", value="Authorization", required=false)
							 	 	@RequestHeader(value="Authorization") String token,
							 	 	@ApiParam(name="tokenType", value="tokenType : admin or null", required=false)
							 	 	@RequestHeader(value="tokenType", required=false) String tokenType,
							 	 	@ApiParam(name="extrMemberSequence", value="요청한 사용자", required=false)
							 	 	@RequestParam (required=false) Long extrMemberSequence,
							 	 	@ApiParam(name="operationType", value="operationType", required=false) 
									@RequestParam(required=false) AcpOperationType operationType,
									@ApiParam(name="resourceUri", value="리소스Uri", required=false) 
									@RequestParam(required=false) String resourceUri,
							 	 	@ApiIgnore
 	 								Messages messages) {
	  log.debug("접근 가능한 권한 정책 목록 조회 : token {},  tokenType {},  extrMemberSequence {}, operationType {}, resourceUri {}", token, tokenType, extrMemberSequence, operationType, resourceUri);
	  
	  Long memberSequence = accessConrtolService.getMemberFromToken(token,tokenType,extrMemberSequence);
	  
	  log.info("토큰 정보 확인 token : {}, tokenType : {},  extrMember :{}, memberSequence : {}", token, tokenType, extrMemberSequence, memberSequence);
	  
	  
	  messages.addMessage(Const.RESPONSE_CODE_OK, "");
	  
	  return accessConrtolService.retrieveAccessControlPolicies(memberSequence, operationType, resourceUri);
	  
	}
	
	@ApiOperation(value = "특정 가능한 권한 정책 조회", notes = "특정 권한 정책을 조회합니다.", httpMethod = "GET")
    @GetMapping(value="/{acpId}")
	private AccessControl retrieveAccessControlPolicy(
									@ApiParam(name="Authorization", value="Authorization", required=false)
							 	 	@RequestHeader(value="Authorization") String token,
							 	 	@ApiParam(name="tokenType", value="tokenType : admin or null", required=false)
							 	 	@RequestHeader(value="tokenType", required=false) String tokenType,
							 	 	@ApiParam(name="extrMemberSequence", value="요청한 사용자", required=false)
							 	 	@RequestParam (required=false) Long extrMemberSequence,
									@ApiParam(name="acpId", value="acp id", required=true) 
									@PathVariable(value="acpId") String accessControlId,
							 	 	@ApiIgnore
 	 								Messages messages) {
	  log.debug("특정 접근 가능한 권한 정책 조회 : token {},  tokenType {},  extrMemberSequence {}, accessControlId {}", token, tokenType, extrMemberSequence, accessControlId);
	  
	  Long memberSequence = accessConrtolService.getMemberFromToken(token,tokenType,extrMemberSequence);
	  
	  log.info("토큰 정보 확인 token : {}, tokenType : {},  extrMember :{}, memberSequence : {}", token, tokenType, extrMemberSequence, memberSequence);
	  
	  
	  messages.addMessage(Const.RESPONSE_CODE_OK, "");
	  
	  return  accessConrtolService.retrieveAccessControlPolicy(memberSequence, accessControlId);

	  
	}
	
	@ApiOperation(value = "리소스 접근 권한 생성", notes = "리소스 접근 권한을 생성합니다.(권한 부여)", httpMethod = "POST")
    @RequestMapping(value="/{acpId}", method=POST)
	private AccessControl registerAccessControl(
							 	 	@ApiParam(name="Authorization", value="token", required=false)
							 	 	@RequestHeader(value="Authorization") String token,
							 	 	@ApiParam(name="tokenType", value="tokenType : admin or null", required=false)
							 	 	@RequestHeader(value="tokenType", required=false) String tokenType,
							 	 	@ApiParam(name="acpId", value="사용자가 입력하는 acpId", required=true)
							 	 	@PathVariable(value = "acpId") String acpId,
							 	 	@ApiParam(name="extrMemberSequence", value="권한을 부여할 사용자", required=false)
							 	 	@RequestParam (required=false) Long extrMemberSequence,
							 	 	@ApiParam(name="acp", value="권한 정보", required=true)
							 	 	@RequestBody (required=false) AccessControl accessControl,
							 	 	@ApiIgnore
 	 								Messages messages) {

	  log.info("입력 받은 권한 정보 확인... acp : {},",accessControl!=null?accessControl.toString():"accessContro is null");
	  
	  Long memberSequence = accessConrtolService.getMemberFromToken(token,tokenType,extrMemberSequence);
	  log.info("토큰 정보 확인 token : {}, tokenType : {}, acpId :{}, extrMember :{}, memberSequence : {}", token, tokenType, acpId, extrMemberSequence, memberSequence);
	  
	  accessControl.setGrantor(memberSequence);
	  accessConrtolService.validAccessControl(accessControl);
	  
	  //parsingResourceUrl
	  ResourceInfo info = resourceParsingUtil.parsingResourceUrl(accessControl.getResourceUriList().get(0));
			
	  if(info == null) {
		//없는 url로 요청 -> error 리턴 
		throw new BadRequestException(ResponseCode.INTERNAL_INVALID_PARAMETER);
	  }
	  log.debug("memberSequence : {}",memberSequence);
	  accessConrtolService.checkAccessControlOfResourceList(accessControl.getResourceUriList(),memberSequence, AcpOperationType.GRANT,info);

	  accessControl.setId(acpId);
	  AccessControl result  = accessConrtolService.registerAccessControl(memberSequence, accessControl);

	  messages.addMessage(Const.RESPONSE_CODE_OK, "");
	  
	  return accessConrtolService.getResultFormat(result);
	  
	}

	@ApiOperation(value = "리소스 접근 권한 삭제", notes = "리소스 접근 권한을 삭제합니다.", httpMethod = "DELETE")
    @RequestMapping(value="/{acpId}", method=DELETE)
	private void deleteGrantedDeviceAccessControl(
							 	 	@ApiParam(name="token", value="token", required=false)
							 	 	@RequestHeader(value="Authorization") String token,
							 	 	@ApiParam(name="tokenType", value="tokenType : admin or null", required=false)
							 	 	@RequestHeader(value="tokenType", required=false) String tokenType,
							 	 	@ApiParam(name="extrMemberSequence", value="권한을 부여할 사용자", required=false)
							 	 	@RequestParam (required=false) Long extrMemberSequence,
							 	 	@ApiParam(name="acpId", value="삭제 할 acp id", required=true)
							 	 	@PathVariable String acpId,
							 	 	@ApiIgnore
 	 								Messages messages) {

			
	  log.info("입력 받은 권한 정보 확인... acp id: {}, tokenType : {}, extrMemberSequence : {}, token : {}",acpId, tokenType, extrMemberSequence, token); 
		 
	  Long memberSequence = accessConrtolService.getMemberFromToken(token,tokenType,extrMemberSequence);
	  log.info("토큰 정보 확인 token : {}, tokenType : {}, acpId :{}, extrMember :{}, memberSequence : {}", token, tokenType, acpId, extrMemberSequence, memberSequence);
	  
	  
	  AccessControl accessControl = new AccessControl();
	  accessControl.setId(acpId);
	  accessControl.setGrantor(memberSequence);
	  
//	  accessConrtolService.retrieveAndDeleteAccessControl(accessControl, memberSequence);
	  accessConrtolService.deleteAccessControl(accessControl, memberSequence);
	  
	  messages.addMessage(Const.RESPONSE_CODE_OK, "");

	}
	@ApiOperation(value = "리소스 접근 권한 수정", notes = "리소스 접근 권한을 수정합니다.", httpMethod = "PUT")
    @RequestMapping(value="/resources/grant/{acpId}", method=PUT)
	private void updateGrantedDeviceAccessControl(
							 	 	@ApiParam(name="token", value="token", required=false)
							 	 	@RequestHeader(value="Authorization") String token,
							 	 	@ApiParam(name="tokenType", value="tokenType : admin or null", required=false)
							 	 	@RequestHeader(value="tokenType", required=false) String tokenType,
							 	 	@ApiParam(name="memberSequence", value="권한을 부여할 사용자", required=false)
							 	 	@RequestParam (required=false) Long extrMemberSequence,
							 	 	@ApiParam(name="acpId", value="삭제 할 acp id", required=true)
							 	 	@PathVariable String acpId,
							 	 	@ApiParam(name="acp", value="권한 정보", required=true)
							 	 	@RequestBody (required=false) AccessControl accessControl,
							 	 	@ApiIgnore
 	 								Messages messages) {

			
	  log.info("입력 받은 권한 정보 확인... acp : {},", accessControl!=null?accessControl.toString():"accessContro is null"); 
		 
	  Long memberSequence = accessConrtolService.getMemberFromToken(token,tokenType,extrMemberSequence);
	  
	  log.info("토큰 정보 확인 token : {}, tokenType : {}, acpId :{}, extrMember :{}, memberSequence : {}", token, tokenType, acpId, extrMemberSequence, memberSequence);
	  
	  accessConrtolService.validAccessControl(accessControl);
	  
		ResourceInfo info = resourceParsingUtil.parsingResourceUrl(accessControl.getResourceUriList().get(0));
			
			if(info == null) {
				//없는 url로 요청 -> error 리턴 
				throw new BadRequestException(ResponseCode.INTERNAL_INVALID_PARAMETER);
			}
	  
	  //요청한 사용자가 해당 리소스 삭제 권한 있나 확인
	  accessConrtolService.checkAccessControlOfResourceList(accessControl.getResourceUriList(),memberSequence, AcpOperationType.UPDATE, info);
	  accessControl.setId(acpId);
	  //FIXME update 위해서 새 request body 객체를 만들어야 한다.
	  //accessConrtolService.retrieveAndDeleteAccessControl(accessControl, memberSequence);
	  
	  messages.addMessage(Const.RESPONSE_CODE_OK, "");

	}
	
	
}
