package com.kt.iotmakers.platform.api.v2.accessControl.mapper;

import java.util.List;
import java.util.Map;

import com.kt.iotmakers.platform.api.v2.accessControl.vo.AccessControl;
import com.kt.iotmakers.platform.api.v2.accessControl.vo.AcpDetail;
import com.kt.iotmakers.platform.api.v2.accessControl.vo.TimeContextFactor;

public interface AccessControlMapper {

	public List<AccessControl> checkUserCanDoToResource(Map<String, Object> param);
	public String retrieveUsedAcp(Map<String, Object> param);
	public void insertAcp(AccessControl acp);
	public void insertAcpDetails(List<AcpDetail> details);
	public void insertResourceMapping(AccessControl acp);
	public List<AccessControl> retrieveStrictAcpList(Map<String, Object> params);
	public int deleteAcpResource(Map<String, Object> params);
	public int deleteAcp(Map<String, Object> params);
	public int retrieveRemainResource(Map<String, Object> params);
	public List<AccessControl> retrieveGrantedAcp(Map<String, Object> params);
	public List<AccessControl> retrieveAllResource(Map<String, Object> params);
	
	public void insertTimeContextFactor(Map<String, Object> params);
	public TimeContextFactor retrieveTimeContext(Map<String, Object> params);
	public void updateTimeContextExpired(Map<String, Object> map);
//	public List<Map<String, Object>> retrieveAllRelatedResources(Map<String, Object> params);
	/**
	 * resourceUri로 매핑된 전체 ACP_ID 및 detail 조회
	 */
	public List<AccessControl> retrieveAllRelatedResources(Map<String, Object> params);
	public String retrieveUsedAcpWithMember(Map<String, Object> map);
	public int deleteAllRelatedOwnerResources(Map<String, Object> params);
	public int deleteContexts(Map<String, Object> params);
	public List<TimeContextFactor> retrieveTimeContextForSchedule();
	
	
	public List<AccessControl> retrieveAccessControlPolicies(Map<String, Object> params);

	public int changeAcpOwner(AccessControl tmp);
	public int deleteAllRelatedResources(Map<String, Object> params);
	public boolean acpIdExists(String acpId);
	public AccessControl retrieveAccessControlPoliciy(Map<String, Object> params);
	public AccessControl retrieveOwnerAcp(AccessControl accessControl);

	
}
