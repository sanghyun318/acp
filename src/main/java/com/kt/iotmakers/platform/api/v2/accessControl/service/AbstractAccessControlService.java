package com.kt.iotmakers.platform.api.v2.accessControl.service;

import java.security.SecureRandom;
import java.util.List;
import java.util.Random;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kt.iotmakers.platform.Const;
import com.kt.iotmakers.platform.api.v2.accessControl.vo.AccessControl;
import com.kt.iotmakers.platform.api.v2.accessControl.vo.AcpDetail;
import com.kt.iotmakers.platform.api.v2.accessControl.vo.AcpOperationType;
import com.kt.iotmakers.platform.api.v2.accessControl.vo.AcpType;
import com.kt.iotmakers.platform.api.v2.accessControl.vo.ContextFactor;
import com.kt.iotmakers.platform.base.exception.BadRequestException;
import com.kt.iotmakers.platform.base.message.ResponseCode;
import com.kt.iotmakers.platform.base.util.ObjectConverter;
import com.kt.iotmakers.platform.config.properties.ResourceInfo;

public abstract class AbstractAccessControlService {
	
	
	protected static final String defautAcpName = "ACP";
	private static final String tokenMbrSequeunce = "mbr_seq";
	
	private static Logger logger = LoggerFactory.getLogger(AbstractAccessControlService.class);
	
	
	protected boolean validOperationType(String type){
		try {
			AcpOperationType.valueOf(type.toUpperCase());
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("type is not invalid : {}", type);
			return false;
		}
		return true;
	}
	
	protected JSONObject parsingAccessToken(String token){
		
		JSONObject jsonObject = null;
		if(token == null)
			return null;
		try {
		Jwt jwt = JwtHelper.decode(token);
		
			jsonObject = ObjectConverter.toJSON(jwt.getClaims());
			
		} catch (JsonProcessingException | ParseException | IllegalArgumentException e ) {
			// TODO Auto-generated catch block
			logger.error(e.toString());
			throw new BadRequestException(ResponseCode.INTERNAL_INVALID_PARAMETER);
		}
		return jsonObject;
	}
	/**
	 * access token에서 회원 일련번호 추출
	 * @since 2019. 5. 10.
	 * @author SanghyunLee
	 * @param token
	 * @return memberSequenc
	 */
	public Long extractMemberFomToken (String token){
		
		JSONObject object = parsingAccessToken(token);
		if(object != null){
			Long memberSequence = Long.valueOf((String) object.get(tokenMbrSequeunce));
			return memberSequence;
		}
		return null;
	}
	
	/**
	 * FIXME admin token 확인 로직 개선 필요 
	 * @author : SanghyunLee
	 * @return : boolean
	 * @desc   : 토큰이 admin 토큰인지 확인 
	 * @param token
	 * @param tokenType
	 * @return
	 */
	public boolean isAdminToken(String token, String tokenType) {
		
		if(Const.TOKEN_TYPE_ADMIN.equals(tokenType) && Const.TOKEN_ADMIN.equals(token))
			return true;
		return false;	
	}
	
	/**
	 * 특정 리소스에 대한 사용자의 접근 권한을 확인함
	 * @since 2019. 5. 10.
	 * @author SanghyunLee
	 * @param resourceUrl
	 * @param memberSequence
	 * @param operation
	 * @return
	 */
	public abstract Boolean checkResource(String resourceUrl, Long memberSequence, AcpOperationType operation);
	
	/**
	 * ACP가 없는 경우 ACP를 생성함
	 * @since 2019. 5. 10.
	 * @author SanghyunLee
	 */
	protected void genernateAcp(){
		
	}
	private final String SOURCES =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

	/**
     * Generate a random string.
    * @param random the random number generator.
    * @param characters the characters for generating string.
    * @param length the length of the generated string.
    * @return
    */
   private String generateString(Random random, String characters, int length) {
       char[] text = new char[length];
       for (int i = 0; i < length; i++) {
           text[i] = characters.charAt(random.nextInt(characters.length()));
       }
       return new String(text);
   }
  /**
   *  
   * Master Acp Id 생성 로직 = acp : {acpType} : random 10 char : System.currentTimeMillis()/100
   * Granted Acp  Id 생성 로직 = acp : 외부에서 입력 받은 acp id 
   * @author : SanghyunLee
   * @return : String
   * @desc   : 
   * @param key
   * @return
   */
	protected String genAcpId(AcpType acpType) {

		StringBuilder builder = new StringBuilder(Const.GEN_ACP_ID_PREFIX);
		builder.append(Const.ACP_DELIMITER);
		builder.append(acpType.getValue()).append(Const.ACP_DELIMITER);
		String val = generateString(new SecureRandom(), SOURCES, 10) + Const.ACP_DELIMITER
				+ (System.currentTimeMillis() / 100); // 일부러
		builder.append(val);
		return builder.toString();
	}
  /**
   *  
   * Master Acp Id 생성 로직 = acp : {acpType} : random 10 char : System.currentTimeMillis()/100
   * Granted Acp  Id 생성 로직 = acp : 외부에서 입력 받은 acp id 
   * @author : SanghyunLee
   * @return : String
   * @desc   : 
   * @param key
   * @return
   */
  protected String genAcpId(String id) {
	  
	  //TODO 외부에서 id를 입력 받아 처리하는 케이스에 로직 변경 시 여기에 로직 추가 
		return id;
	}
  
	public abstract void grantAccessToResource(Long grantor, Long grantee, String resourceUrl, AcpOperationType operation);
	
	public abstract List<String> getResourceUriList(String resourceUrl);

	public abstract Boolean checkResource(String resourceUrl, Long memberSequence, AcpOperationType operation,
			ResourceInfo info);
	
	protected abstract AccessControl makeAccessControl(AcpType acpType, Long grantor, List<Long> originators, List<AcpOperationType> operations, List<String> resourceUriList, List<ContextFactor> contextList, String extrIdVal, String acpName);
	
	protected AcpDetail makeAcpDetail(String acpId, List<Long> originators, List<AcpOperationType> operations) {
		AcpDetail detail  = new AcpDetail();
		detail.setId(acpId);
		detail.setOriginator(originators);
		detail.setOperationType(operations);
		return detail;
		
	}
	
}
