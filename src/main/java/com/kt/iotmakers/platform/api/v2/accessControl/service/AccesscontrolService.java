package com.kt.iotmakers.platform.api.v2.accessControl.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kt.iotmakers.platform.Const;
import com.kt.iotmakers.platform.api.v2.accessControl.mapper.AccessControlMapper;
import com.kt.iotmakers.platform.api.v2.accessControl.vo.AccessControl;
import com.kt.iotmakers.platform.api.v2.accessControl.vo.AcpDetail;
import com.kt.iotmakers.platform.api.v2.accessControl.vo.AcpDetailType;
import com.kt.iotmakers.platform.api.v2.accessControl.vo.AcpOperationType;
import com.kt.iotmakers.platform.api.v2.accessControl.vo.AcpType;
import com.kt.iotmakers.platform.api.v2.accessControl.vo.ContextFactor;
import com.kt.iotmakers.platform.api.v2.accessControl.vo.TimeContextFactor;
import com.kt.iotmakers.platform.base.exception.BadRequestException;
import com.kt.iotmakers.platform.base.message.BaseResponse;
import com.kt.iotmakers.platform.base.message.ResponseCode;
import com.kt.iotmakers.platform.base.util.HttpUtil;
import com.kt.iotmakers.platform.base.util.ResourceParsingUtil;
import com.kt.iotmakers.platform.config.properties.ResourceInfo;
import com.kt.iotmakers.platform.config.properties.ResourceValidType;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class AccesscontrolService extends AbstractAccessControlService{

	
	@Autowired
	private AccessControlMapper accessControlMapper;
	
	@Autowired
	private ResourceParsingUtil resourceParsingUtil;
	
	@Autowired
	private HttpUtil httpUtil;
	
	@Override
	public Boolean checkResource(String resourceUrl, Long memberSequence, AcpOperationType operation, ResourceInfo info) {
		// TODO Auto-generated method stub
		log.info("in DeviceAccesscontrolService: checkResource : resourceUrl:{}, memberSeq:{},AcpOperationType:{}",resourceUrl,memberSequence,operation);
		/*
		 * 리소스 생성 시 안만들고
		 * 확인 요청 들어오면 상위 리소스를 확인하여 처리함
		 * 권한 부여가 발생하는 경우에 해당 url에 대해 리소스 추가
		 *  
		 */
		AccessControl result = checkResourceWithInfoAndRetry(resourceUrl, info, memberSequence, operation);
		if(result != null && result.getResult() == true) return true;
		else 
			return false;
	}

	@Override
	public void grantAccessToResource(Long grantor, Long grantee, String resourceUrl, AcpOperationType operation) {
		// TODO Auto-generated method stub
		
	}
	
	public List<String> getResourceUriList(String resourceUrl) {
		log.info("in getResourceUriList : {}",resourceUrl);
		
		if(resourceParsingUtil.validResourceUrl(resourceUrl))
			return  Arrays.asList(resourceUrl);
		else {
			//valid 하지 않은 url
			log.error("in valid resourceUrl : {}",resourceUrl);
			throw new BadRequestException(ResponseCode.INTERNAL_INVALID_PARAMETER);
		}
	}

	public AcpOperationType validOperation(String type){
		
		if(super.validOperationType(type)){
			return AcpOperationType.valueOf(type.toUpperCase());
		}else {
			throw new BadRequestException(ResponseCode.INTERNAL_INVALID_PARAMETER);
		}
	}
	/**
	 * 
	 * @author : SanghyunLee
	 * @return : void
	 * @desc   : 회원의 MasterAcp에 리소스를 매핑 시킴 
	 * @param memberSequence
	 * @param resourceUri
	 * @param className
	 */
	public void registerResource(Long memberSequence, String resourceUri, Class<?> className){
		
		log.debug("in registerResource :member {}, resource {}, Class:{}",memberSequence,resourceUri,className==null?null:className.getSimpleName());
		//FIXME !!!기존에 존재하는 해당 resourceUri에 대해 MasterAcp를 가지고 있는것 삭제처리 음... 정책적으로 이게 맞을까..???? -> ㅇ
		deleteOldResource(resourceUri);
		
		
		Map<String, Object> param = new HashMap<>();
		param.put(Const.GRANTOR, memberSequence);
		param.put(Const.ORIGINATOR, new ArrayList<>(Arrays.asList(memberSequence)));
		param.put(Const.ACP_TYPE, AcpType.RESOURCE_OWNER.getValue());//owner
		param.put(Const.OPERATION_TYPE, new ArrayList<>(Arrays.asList(AcpOperationType.ALL.getValue())));//ALL
		
		//해당 사용자가 기존에 가지고 있던 MasterAcp가 있으면 조회
		log.debug("in checkAcp param:{}",param);
		String existedAcpId = accessControlMapper.retrieveUsedAcp(param);
		
		log.debug("in result of retrieveUsedAcp :{}",existedAcpId);		
		checkExistingAcpAndMappingResource(existedAcpId, memberSequence, resourceUri);

	}
	
	/**
	 * 
	 * @since  : 2019. 11. 8.
	 * @author : SanghyunLee
	 * @return : void
	 * @desc   : 해당 리소스에 매팽된 모든 권한 매핑을 해제 (이전에 남아 있던 것은 미처 삭제 되지 못한 잔재라고 판단)
	 * @param resourceUri
	 */
	private void deleteOldResource(String resourceUri) {
		Map<String, Object> param = new HashMap<>();
		param.put(Const.RESOURCE_URI, resourceUri);
		accessControlMapper.deleteAllRelatedResources(param);
	}

	/**
	 * Master Acp id, name은 자체 룰에 따라 생성 
	 */
	protected AccessControl makeAccessControl(AcpType acpType, Long grantor, List<Long> originators, List<AcpOperationType> operations, List<String> resourceUriList, List<ContextFactor> contextList, String extrIdVal, String acpName) {
		
		AccessControl accessControl = new AccessControl();
		String acpId = null;
		
		if(AcpType.RESOURCE_GRANTEE.equals(acpType)) {
			acpId = genAcpId(extrIdVal);
		} else {
			//grantee
			acpId = genAcpId(acpType);
		}

		if(acpIdIsExists(acpId)) {
			log.warn("acpId already exists :{}", acpId); // 접근 권한 정책 등록 외에는 자동 생성
			throw new BadRequestException(ResponseCode.INTERNAL_DUPLICATE_PARAMETER);
		}
		accessControl.setId(acpId);
		
		
		if(acpName == null)
			accessControl.setName(genAcpName(grantor,acpType));
		else 
			accessControl.setName(acpName);
		
		accessControl.setType(acpType);
		accessControl.setGrantor(grantor);
		accessControl.setResourceUriList(resourceUriList);
		accessControl.setOriginators(originators);
		
		if(contextList != null && !contextList.isEmpty()) {
			accessControl.setContexts(contextList);
			accessControl.setContextUseYn(true);
		} else {
			accessControl.setContextUseYn(false);
		}
		
		//ACP_DETAIL 생성
		List<AcpDetail> details = new ArrayList<>();
		details.add(makeSpvDetail(acpId,  new ArrayList<>(Arrays.asList(grantor)), new ArrayList<>(Arrays.asList(AcpOperationType.ALL))));
		details.add(makePvDetail(acpId, originators, operations));
		accessControl.setAcpDetails(details);
		
		return accessControl;
	}
	private boolean acpIdIsExists(String acpId) {
		log.info("acpIdIsExists - acpId : {}", acpId);
		
		return accessControlMapper.acpIdExists(acpId);
		
	}

	private AcpDetail makeSpvDetail(String acpId, List<Long> originators, List<AcpOperationType> operations) {
		
		AcpDetail spvDetail = makeAcpDetail(acpId, originators, operations);
		spvDetail.setType(AcpDetailType.SELF_PRIVILEGE);
		return spvDetail;
	}
	private AcpDetail makePvDetail(String acpId, List<Long> originators, List<AcpOperationType> operations) {
		
		AcpDetail pvDetail = makeAcpDetail(acpId, originators, operations);
		pvDetail.setType(AcpDetailType.PRIVILEGE);
		return pvDetail;
	}

	/**
	 * 권한 부여에 사용할 acp가 존재하는지 확인한다.
	 * @since 2019. 5. 14.
	 * @author SanghyunLee
	 * @param grantor
	 * @param operationList
	 * @param originator
	 */
	public void grantAccessControl(Long grantor, List<AcpOperationType> operationList, List<Long> originator, List<String> resourceUriList, List<ContextFactor> contextList) {
		//spv가 grantor, acp_type =01,  operationVale는, pv에 originator만 있어야 함(더 있으면 안됨
		
		Map<String, Object> param = new HashMap<>();
		param.put(Const.GRANTOR, grantor);
		param.put(Const.ORIGINATOR, originator);
		param.put(Const.ACP_TYPE, AcpType.RESOURCE_GRANTEE.getValue());//grant
		param.put(Const.OPERATION_TYPE, convertAcpOprTypeList(operationList));

		log.info("in checkAcp param:{}",param);
		/*
		 *  기존에 동일하게 사용된 acp가 있으면 재사용 (originators, operationType이 동일해야 함)
		 */
		String existedAcpId = accessControlMapper.retrieveUsedAcp(param);
		
		log.info("used acp id : {}, grantor : {}, operationValue:{}, originator :{}",existedAcpId, grantor, operationList, originator);
		AccessControl acp  = null;
		
		/*
		 * 		//acp 생성 -> 
		 * 		[acp_bas]
		 * 			acp_id 네이밍 생성
		 * 			acp_type은 01
		 * 			acp_nm 은 네이밍
		 * 			creator : grantor
		 * 			cret_dt : now()
		 * 
		 * 		[acp_dtl]
		 * 			acp_id
		 * 			acr_type : spv, pv
		 * 			acr_oprs : 00 (all), 01 ()
		 * 			originators : spv {grantor}, pv {originator}
		 */
		//acpId가 null 이면 만들고, 있으면 사용해서 리소스 매핑
		if(existedAcpId == null){
			log.info("grantAccessControl- make new acp !!!!");
			acp = makeAccessControl(AcpType.RESOURCE_GRANTEE, grantor, originator, operationList, resourceUriList, contextList, null, null);

			if(acp != null && acp.getAcpDetails() != null) {
				log.info("generated spvDetail :{}",acp.getAcpDetails().get(0));
				log.info("generated pvDetail :{}",acp.getAcpDetails().get(1));
			}
			log.info("generated acp :{}",acp);
			
			//insert acp
			accessControlMapper.insertAcp(acp);

		} else {
			acp = new AccessControl();
			acp.setResourceUriList(resourceUriList);
			if(contextList != null && !contextList.isEmpty()) {
				acp.setContexts(contextList);
				acp.setContextUseYn(true);
			} else acp.setContextUseYn(false);
			acp.setId(existedAcpId);
		}
		
		//만들어진 acp를 가지고 리소스 매핑 RESRC_BY_ACP_REL
		accessControlMapper.insertResourceMapping(acp);
		
		if(contextList != null) {
			//FIXME  권한 부여 시 context를 받은 경우 처리 로직 함수로 뺄것
			log.info(":::::: {}",contextList.size());
			log.info(":::::: {}",((ContextFactor) contextList.get(0)).toString());
			log.info(":::::: {}",((ContextFactor) contextList.get(0)) instanceof TimeContextFactor);
			log.info(":::::: {}",acp.getResourceUriList());
			
			for(ContextFactor factor : contextList) {
				if(factor instanceof TimeContextFactor) {
					log.info(" this is TimeContextFactor");
					//FIXME TimeContextFactor 등록 로직 함수로 뺄것
					Map<String, Object> contextParam = new HashMap<String, Object>();
					contextParam.put(Const.ACP_ID,acp.getId());
					contextParam.put(Const.ACR_TYPE,AcpDetailType.PRIVILEGE.getValue());
					contextParam.put(Const.TIME_CONTEXT_EXPIRES_AT,((TimeContextFactor) factor).getExpiresAt());
					contextParam.put(Const.RESOURCE_URI_LIST,acp.getResourceUriList());
					accessControlMapper.insertTimeContextFactor(contextParam);
				}
				
			}
		}
		
	}
	/**
	 * acp 이름 생성 로직
	 * @since 2019. 5. 14.
	 * @author SanghyunLee
	 * @param memberSequence
	 * @param acpType
	 * @return
	 */
	private String genAcpName(Long memberSequence, AcpType acpType) {
		
		StringBuilder name = new StringBuilder(Const.ACP_NAME_PREFIX);
		if(AcpType.RESOURCE_OWNER.equals(acpType)){
			log.debug("genAcpName : owner");
			name.append(Const.ACP_OWNER_NAME).append(memberSequence);
			
		} else {
			log.debug("genAcpName : grantee");
			name.append(Const.ACP_GRANTEE_NAME).append(memberSequence);
		}
		log.info("final name : {}",name.toString());
		return name.toString();
	}

	/**
	 * convert List of AcpOperationType to List of String
	 * @since 2019. 5. 14.
	 * @author SanghyunLee
	 * @param operationList
	 * @return
	 */
	private List<String> convertAcpOprTypeList(List<AcpOperationType> operationList) {
		// TODO Auto-generated method stub
		  List<String> operationTypes = new ArrayList<>();
		  
		  for(AcpOperationType type : operationList){
			  operationTypes.add(type.getValue());
		  }
		  log.info("in convertAcpOprTypeList , operationTypes:{}",operationTypes);
		
		return operationTypes;
	}

	
	/**
	 * 인증 토큰 또는 관리자 토큰에서 사용자 정보를 확인하여 리턴함 
	 * @since 2019. 5. 14.
	 * @author SanghyunLee
	 * @param token
	 * @param tokenType
	 * @param extrMemberSequence
	 * @return
	 */
	public Long getMemberFromToken(String token, String tokenType, Long extrMemberSequence) {
		Long memberSequence = null;
		if(token != null){
			if (isAdminToken(token, tokenType)) {

				memberSequence = extrMemberSequence;
			} else {
				token = token.replaceAll(Const.BEARER_PARSING, "");
				memberSequence = extractMemberFomToken(token);
			}
			
			
			if (memberSequence == null) {
				log.warn("memberSequence is null : {}, {}, {} {}",token, tokenType, extrMemberSequence, memberSequence);
				throw new BadRequestException(ResponseCode.INTERNAL_MANDATORY_PARAMETER_MISSING);
			}
			return memberSequence;
	   }else {
		   throw new BadRequestException(ResponseCode.INTERNAL_AUTHENTICATION_FAILED);
	   }
	}
	/**
	 * 입력 받은 resourceUriList에 대해 사용자가 ALL또는 type할 권한 있는지 확인 함
	 * @since 2019. 5. 14.
	 * @author SanghyunLee
	 * @param resourceUriList
	 * @param memberSequence
	 * @param grant
	 */
	public void checkAccessControlOfResourceList(List<String> resourceUriList, Long memberSequence, AcpOperationType type, ResourceInfo info) {
		for (String resource : resourceUriList) {
			if (!checkResource(resource, memberSequence, type, info)) {
				// 사용자가 권한을 부여하려는 대상에 권한이 없음 (ALL 이거나 GRANT 권한이 있어야 권한 부여 가능
				log.error("사용자{}가 권한을 부여하려는 대상{}에 권한이 없음 ", memberSequence, resource);
				throw new BadRequestException(ResponseCode.INTERNAL_AUTHENTICATION_FAILED);
			}
		}
	}
	/**
	 * payload로 입력 받은 AccessControl값을 체크함
	 * @since 2019. 5. 14.
	 * @author SanghyunLee
	 * @param accessControl
	 */
	public void validAccessControl(AccessControl accessControl) {
		
		if(accessControl == null || accessControl.getOperationType() == null || accessControl.getResourceUriList() == null || accessControl.getOriginators() == null || accessControl.getGrantor() == null){
			log.warn("in validAccessControl:{}",accessControl!=null?accessControl.toString():"accessControl is null");
			throw new BadRequestException(ResponseCode.INTERNAL_MANDATORY_PARAMETER_MISSING);
		}
	}

	/**
	 * 
	 * @since  : Jul 23, 2019
	 * @author : SanghyunLee
	 * @return : AccessControl
	 * @desc   : 입력 받은 정보로 한번 권한 조회함 (재시도)
	 * @param resourceUrl
	 * @param info
	 * @param memberSequence
	 * @param operationType
	 * @return
	 */
	private AccessControl checkResourceWithInfo(String resourceUrl, ResourceInfo info, Long memberSequence, AcpOperationType operationType) {
			
		ResourceValidType type = info.getType();
		
		if(ResourceValidType.DB.equals(type)) {
			log.info("check type db");

			// DB로 찾는 함수 호출
			if(checkResource(resourceUrl, memberSequence, operationType)) {
				//true 접근 가능
				return genAccessControl(true, operationType, resourceUrl,memberSequence);
			} else {
				return genAccessControl(false, operationType, resourceUrl,memberSequence);	
			}

		} else if(ResourceValidType.API.equals(type)) {
			log.info("check type API");
			
			//API로 찾는 함수 호출
			if(checkResourceWithAPI(resourceUrl, memberSequence, operationType)) {
				return genAccessControl(true, operationType, resourceUrl,memberSequence);	
			}
			else {
				return genAccessControl(false, operationType, resourceUrl,memberSequence);
			}		
		}
		return null;
	}
	
	public AccessControl checkResourceWithInfoAndRetry(String resourceUrl, ResourceInfo info, Long memberSequence, AcpOperationType operationType) {
		log.debug("checkResourceWithInfoAndRetry  resourceUri : {}, memberSequence :{}, AcpOperationType : {}", resourceUrl, memberSequence, operationType);
		//from controller
		ResourceValidType type = info.getType();
		
		if(ResourceValidType.DB.equals(type)) {
			//FIXME DB로 권한 확인하는 로직 따로 함수로 빼기
			log.info("check type db");

			return checkAccessControlWithDB(resourceUrl, info, memberSequence, operationType);


		} else if(ResourceValidType.API.equals(type)) {
			log.info("check type API");
			//FIXME API로 권한 확인하는 로직 따로 함수로 빼기
			//API로 찾는 함수 호출 (API로 확인하는 경우 요청 받은 모듈에서 상위 리소스 확인하는 로직을 수행하기 때문에 간단
			if(checkResourceWithAPI(resourceUrl, memberSequence, operationType)) {
				return genAccessControl(true, operationType, resourceUrl,memberSequence);	
			}
			else {
				return genAccessControl(false, operationType, resourceUrl,memberSequence);
			}			
		}

		return null;
	}
	/**
	 * 
	 * @author : SanghyunLee
	 * @return : AccessControl
	 * @desc   : DB로 권한 확인을 수행. 
	 * @param resourceUrl
	 * @param info
	 * @param memberSequence
	 * @param operationType
	 * @return
	 */
	private AccessControl checkAccessControlWithDB(String resourceUrl, ResourceInfo info, Long memberSequence,
			AcpOperationType operationType) {
		// DB로 찾는 함수 호출
		if(checkResource(resourceUrl, memberSequence, operationType)) {
			//true 접근 가능
			return genAccessControl(true, operationType, resourceUrl,memberSequence);
		} else {
			//false 접근 불가 -> retry
			AccessControl result =  genAccessControl(false, operationType, resourceUrl,memberSequence);
			
			return retryWithUpperResource(result, resourceUrl, info, memberSequence, operationType);
			
		}
		
	}
	/**
	 * 
	 * @author : SanghyunLee
	 * @return : AccessControl
	 * @desc   : 재시도 과정 일반화, 내부 재귀, 상위 리소스 조회하여 property에 저장된 상위 리소스가 null 이거나 true가 나오면 재귀 탈출
	 * @param result
	 * @param resourceUrl
	 * @param info
	 * @param memberSequence
	 * @param operationType
	 * @return
	 */
	private AccessControl retryWithUpperResource(AccessControl result, String resourceUrl, ResourceInfo info,
			Long memberSequence, AcpOperationType operationType) {
		
		if(checkReTry(result)){
			
			AccessControl secondResult = retryUpperResource(result.getResourceUriList().get(0), info , memberSequence, operationType);
			
			if(secondResult == null) return result; // 상위 리소스로 조회가 안됨
			
			else {
				ResourceInfo upperInfo = resourceParsingUtil.getUpperResourceInfo(info) ;
				return retryWithUpperResource(secondResult, secondResult.getResourceUriList().get(0), upperInfo, memberSequence, operationType);
			}
		}
		return result;
	}

	//재시도 과정을 일반화 전
	@Deprecated
	private AccessControl checkAccessControlWithDB1(String resourceUrl, ResourceInfo info, Long memberSequence,
			AcpOperationType operationType) {
		// DB로 찾는 함수 호출
		if(checkResource(resourceUrl, memberSequence, operationType)) {
			//true 접근 가능
			return genAccessControl(true, operationType, resourceUrl,memberSequence);
		} else {
			//false 접근 불가 -> retry
			AccessControl result =  genAccessControl(false, operationType, resourceUrl,memberSequence);
			if(checkReTry(result)){
				AccessControl secondResult = retryUpperResource(resourceUrl, info, memberSequence, operationType);
				
				if(secondResult == null) return result; // 상위 리소스로 조회가 안됨
				
				
				if(checkReTry(secondResult)) {  // 상위 리소스가 false 이면 재시도
					
					AccessControl thirdResult =  retryUpperResource(secondResult.getResourceUriList().get(0),resourceParsingUtil.getUpperResourceInfo(info) ,memberSequence, operationType);
					if(thirdResult == null) return secondResult;
					return thirdResult;
				}  else return secondResult;
				
			} else return result;

		}
		
	}

	private boolean checkReTry(AccessControl result) {
		
		if(result.getResult() == false) {
			return true;
		}
		else return false; // result.getResult == null or true, null 이면 더이상 하위 리소스를 조회 할 수 없음, 현재 상태를 최종적으로 리턴한다. 
	}

	private boolean checkResourceWithAPI(String resourceUrl, Long memberSequence, AcpOperationType operationType) {
		
		ResourceInfo info = resourceParsingUtil.getResourceInfo(resourceUrl);
		if(info == null) {
			//잘못된 resourceUrl로 인한 에러
			throw new BadRequestException(ResponseCode.INTERNAL_INVALID_PARAMETER);
		}
		if(info.getRequestUrl() == null) {
			//음... 추후 새로운 에러 코드 추가..?
			throw new BadRequestException(ResponseCode.INTERNAL_META_DATA_ERROR);
		}
		
		log.info("in checkResourceWithAPI : getRequestUrl {}",info.getRequestUrl()); //localhost:8280/accessControl/v2/target/{targetSequence}/device/{deviceSequence}
		log.info("in checkResourceWithAPI : resourceUrl {}",resourceUrl); ///target/1004/device/1/resource/power-switch
		Map<String, String > param = new HashMap<String, String>();
		param.put(Const.OPERATION_TYPE,operationType.name());
		param.put(Const.EXTR_MEMBER_SEQUENCE,String.valueOf(memberSequence));
	
		Map<String, Object > headerParam = new HashMap<String, Object>();
		
		setAdminToken(headerParam);
		
		
		RequestEntity<BaseResponse<AccessControl>> requestEntity = httpUtil.genRequestEntity(HttpMethod.GET, httpUtil.getUri(info.getRequestUrl(),resourceUrl,param),null,headerParam);
    	
		ResponseEntity<BaseResponse<AccessControl>> result = httpUtil.sendRequest(requestEntity, new ParameterizedTypeReference<BaseResponse<AccessControl>>() {
		}) ;
		
		log.info("result status code: {}",result.getStatusCodeValue());
		BaseResponse<AccessControl> responseBody = result.getBody();

		if(responseBody != null && responseBody.getData() != null) {
			log.info("resposenCode : {}",responseBody.getResponseCode().toString());
			if(Boolean.TRUE.equals(responseBody.getData().getResult()))
				return true;		 
		}
		return false;
		
	}
	private void setAdminToken(Map<String, Object> headerParam) {

		headerParam.put(HttpHeaders.AUTHORIZATION,Const.TOKEN_ADMIN);
		headerParam.put(Const.TOKEN_TYPE,Const.TOKEN_TYPE_ADMIN);
	}

	private AccessControl genAccessControl(boolean result, AcpOperationType type, String resourceUrl, Long memberSequence) {
		AccessControl accessControl = new AccessControl();
		  accessControl.setResult(result);
		  accessControl.setOperationType(new ArrayList<>(Arrays.asList(type)));
		  accessControl.setResourceUriList(new ArrayList<>(Arrays.asList(resourceUrl)));
		  accessControl.setOriginators(new ArrayList<>(Arrays.asList(memberSequence)));
		  return accessControl;
	}

	/**
	 * 해당 권한을 삭제함
	 * Strict version (현재 적용 버전)
	 * RESRC_BY_ACP_REL, ACP_BAS, ACP_DTL을 join 하여 실제 동일한 권한이 있나 확인 
	 * 있으면 삭제, 없으면 noData 리턴
	 * 
	 * (old version -> strict 하게 관리함)
     * 1. 우선 사용자가 입력한 파라미터에 대해 리소스가 DB에 있나 확인 있으면 해당 ACP 조회함
     * 2. 조회된 ACP의 PV 확인 
     * -> ACP에 PV가 자기밖에 없으면 리소스랑 ACP 삭제
	 * -> ACP에 PV에 자기말고 더 있으면 리소스는 그대로 두고 ACP에서 자기만 PV에서 뺀다.
	 * 
	 * @since Jun 26, 2019
	 * @author SanghyunLee
	 * @param accessControl : 요청한 사용자가 삭제를 하려고 하는 권한
	 * @param grantor : 요청한 사용자
	 */
	@Deprecated
	public void retrieveAndDeleteAccessControl(AccessControl accessControl, Long grantor) {
		log.info("in retrieveAndDeleteAccessControl : {}, {}",accessControl.toString(), grantor);
		 
		List<AccessControl> acpList = retrieveStrictAcpList(accessControl, grantor, AcpType.RESOURCE_GRANTEE);

		if(acpList == null || acpList.isEmpty()) {
			//이러한 acp - resource가 DB에 없음
			throw new BadRequestException(ResponseCode.INTERNAL_NO_DATA);
		}
		
		boolean checkAllVal = checkAllOperationType(accessControl.getOperationType(), acpList);
		
		log.info("acpList : {}, allVal: {}",acpList.toString(),checkAllVal);
		if(!checkAllVal) {
			//삭제하지 않는 경우, no data
			throw new BadRequestException(ResponseCode.INTERNAL_NO_DATA);
		}
		
		for(AccessControl acp : acpList) {

			deleteAccessControl(acp.getId(), grantor, AcpType.RESOURCE_GRANTEE, accessControl.getResourceUriList(), acp.getContextUseYn());
		}
		//	checkPiveilegeOtherExist(acp, originators);
	}
	/**
	 * 입력 받은 acccessControl (acp id)와 grantor 정보로 해당 접근 권한을 삭제합니다
	 * @since  : 2019. 11. 14.
	 * @author : SanghyunLee
	 * @return : void
	 * @desc   : 
	 * @param accessControl
	 * @param grantor
	 */
	public void deleteAccessControl(AccessControl accessControl, Long grantor) {
		accessControl.setGrantor(grantor);
		log.info("in deleteAccessControl : {}, {}",accessControl, grantor);
		
		//요청자가 spv 권한 있는지 확인 후 
		AccessControl data = accessControlMapper.retrieveOwnerAcp(accessControl);
		log.info("result acp to delete : {}", data);
		if(data == null) {
			//권한 또는 acp가 없음
			log.warn("no required accessControl");
			throw new BadRequestException(ResponseCode.INTERNAL_AUTHENTICATION_FAILED);
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Const.ACP_ID, accessControl.getId());
		
		//context 삭제
		accessControlMapper.deleteContexts(params);
	
		//리소소 매핑 해제
		int deleteResult = accessControlMapper.deleteAcpResource(params);
		log.debug("deleteAcpResource result : {}", deleteResult);

		//acp 삭제
		deleteResult = accessControlMapper.deleteAcp(params);
		log.debug("deleteAcp result : {}", deleteResult);
	}
	/**
	 * DB에서 조회한 acpList의 oeprationType 와 삭제하려는 operationType을 비교 (ALL 때문)
	 *  
	 * @since Jun 29, 2019
	 * @author SanghyunLee
	 * @param operationType
	 * @param acpList
	 */
	private boolean checkAllOperationType(List<AcpOperationType> operationTypes, List<AccessControl> acpList) {
		
		for(AccessControl acp : acpList) {
			List<AcpOperationType> operationTypeList = acp.getOperationType();
			if(operationTypeList.contains(AcpOperationType.ALL)) {
				log.info("contains all --> needs to check operationTypes detail....");
				if(operationTypes.contains(AcpOperationType.ALL)) {
					//정상, 삭제 가능
					return true;
				}else 
					return false;
			}
		}
		return true;
	}

	private void deleteAccessControl(String acpId, Long grantor, AcpType resourceGrantee, List<String> resourceUriList, Boolean contextUseYn) {
		
		log.info("in deleteAccessControl {}, grantor :{}, type :{}, resourceUriList : {}, contextUseYn : {}",acpId, grantor, resourceGrantee.getValue(), resourceUriList, contextUseYn);
		Map<String, Object> params = new HashMap<String, Object>();

		params.put(Const.ACP_ID,acpId);
		params.put(Const.RESOURCE_URI_LIST,resourceUriList);

		//context가 있으면 contet 삭제
		if(contextUseYn) {
			log.info("delete contexts : {}", params);
			accessControlMapper.deleteContexts(params);
		}
		accessControlMapper.deleteAcpResource(params);
		/*
		 * 해당 acpId에 더이상 매핑된 resource가 없으면 acp 삭제
		 */
		int cnt = accessControlMapper.retrieveRemainResource(params);
		log.info("cnt :{}",cnt);
		if(cnt == 0) {
			//acp 삭제
			accessControlMapper.deleteAcp(params);
		}
	}

//	/**
//	 * 해당 ACP의 originators와 입력 받은 originator를 비교하여 더 있는지 비교 (더 있으면 true, 더 없으면 false)
//	 * @since Jun 26, 2019
//	 * @author SanghyunLee
//	 * @param accessControl
//	 * @param originators
//	 */
//	private void checkPiveilegeOtherExist(AccessControl accessControl, List<Long> originators) {
//		if(accessControl == null || originators == null) {
//			throw new BadRequestException(ResponseCode.INTERNAL_INVALID_PARAMETER);
//		}
//		log.info("in checkPiveilegeOtherExist : {} , originators: {}",accessControl.toString(), originators.toString());
//
//	}
	
	/**
	 * 해당 조건에 맞는 acp 를 리턴함
	 * @since Jun 26, 2019
	 * @author SanghyunLee
	 * @param accessControl : 요청한 사용자가 삭제를 하려고 하는 권한
	 * @param grantor : 요청한 사용자 
	 * @return DB에서 조회한 권한 목록
	 */
	private List<AccessControl> retrieveStrictAcpList(AccessControl accessControl, Long grantor, AcpType acpType) {
		log.info("in retrieveAcpList : {}, {}",accessControl.toString(), grantor);
		Map<String, Object> params = new HashMap<String, Object>();

		params.put(Const.GRANTOR,grantor);
		params.put(Const.ORIGINATORS,accessControl.getOriginators());
		params.put(Const.RESOURCE_URI_LIST,accessControl.getResourceUriList());
		params.put(Const.ACP_ID,accessControl.getId());
		if(acpType != null)
			params.put(Const.ACP_TYPE,acpType.getValue()); // 권한 받은 것 조회
		
		List<AcpOperationType> operationTypes = accessControl.getOperationType();
		
		params.put(Const.OPERATION_TYPE, operationTypes);
		
		return accessControlMapper.retrieveStrictAcpList(params);
		
	}
	
	public List<AccessControl> retrieveGrantedAccessControl(Long grantor, AcpOperationType operationType) {
		
		log.info("in retrieveGrantedAccessControl : {}, {}",grantor, operationType!=null?operationType.getValue():"operationType is null");
		Map<String, Object> params = new HashMap<String, Object>();

		params.put(Const.GRANTOR,grantor);
		params.put(Const.OPERATION_TYPE,operationType);
		params.put(Const.ACP_TYPE,AcpType.RESOURCE_GRANTEE);

		List<AccessControl> list = accessControlMapper.retrieveGrantedAcp(params);
		
		for(AccessControl acp : list) {
			if(acp.checkUseContext()) {
//				log.info("context type : {}", acp.getContexts());
				//FIXME context 여러개 처리 가능하도록
//				switch(acp.getContexts().get(0).getType())
				log.info("context 조사해서 리턴해야 함  : {}",acp.getId());
				
				acp.setContexts(Arrays.asList(getTimeContext(acp.getId())));
			}
			
		}
		log.info("count : {}",list!=null?list.size():"list is empty");
		log.info("result of retrieveGrantedAcp : {}", list!=null?list.toString():"list is null");
		return list;
	}
	/**
	 * 
	 * @author : SanghyunLee
	 * @return : AccessControl
	 * @desc   : 입력 받은 resourceUri에서 상위 리소스를 만들고 상위리소스로 다시 권한을 확인한다.
	 * @param resourceUri
	 * @param info
	 * @param memberSequence
	 * @param operationType
	 * @return
	 */
	private AccessControl retryUpperResource(String resourceUri, ResourceInfo info, Long memberSequence, AcpOperationType operationType) {
		log.debug("in retryUpperResource : {}, {}, {}, {}",resourceUri, info.toString(), memberSequence, operationType.getValue());
		
		ResourceInfo upperInfo = resourceParsingUtil.getUpperResourceInfo(info);
		
		String chagedUrl = resourceParsingUtil.parsingUrlToUpperResource(resourceUri, info);
		if(chagedUrl == null || "".equals(chagedUrl)) {
			log.warn("chagedUrl is null");
			return null;
		}
		log.debug(chagedUrl);
		
		return checkResourceWithInfo(chagedUrl, upperInfo, memberSequence, operationType);

	}

	@Override
	public Boolean checkResource(String resourceUrl, Long memberSequence, AcpOperationType operation) {
		// 	//db 조회
		Map<String, Object> param = new HashMap<String, Object>();
		param.put(Const.MEMBER_SEQUENCE, memberSequence);
		param.put(Const.OPERATION_TYPE, operation.getValue());
		param.put(Const.RESOURCE_URI_LIST, getResourceUriList(resourceUrl));
		
		List<AccessControl> list =  accessControlMapper.checkUserCanDoToResource(param);
		log.debug("count list : {}", list);
		if(list != null && list.size() > 0) {
			
			for(AccessControl tmp : list) {
				log.debug(" in tmp  :{}", tmp);
				if(Boolean.TRUE.equals(tmp.getContextUseYn())){
					log.debug(">>>>>>>> check Context >>>>>>>>");
					if(checkTimeContext(tmp.getId())) {
						log.debug("not expired..... ");
						return true;
					}					
					log.debug("expired..... ");
					return false;
				} else {
					//context를 사용하지 않고 권한이 있음
					return true;
				}
			}//for
			return true;
		}
		return false;
	}

	private void setTimeExpired(Map<String, Object> map) {
		log.info("setTimeExpired : {}", map);
	
		accessControlMapper.updateTimeContextExpired(map);
		
	}

	/**
	 * 
	 * @since  : Jul 17, 2019
	 * @author : SanghyunLee
	 * @return : true : 만료되지 않음, false : 만료됨
	 * @desc   : 일단 해당 operation이 가능함. 그런데 context를 사용한다고 하니 한번더 체크한다. 
	 * @param id
	 * @param resourceUrl
	 */
	private boolean checkTimeContext(String id) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(Const.ACP_ID, id);
		map.put(Const.ACR_TYPE, Const.ACR_TYPE_PV);
		TimeContextFactor timeContextFactor = accessControlMapper.retrieveTimeContext(map);
		log.info(" checkTimeContext : {}, expired??? {}", timeContextFactor, timeContextFactor.checkExpired());
		if(timeContextFactor.getExpiredYn()) return false;
		
		if(timeContextFactor.checkExpired()) {
			//update 함
			setTimeExpired(map);
			return false;
		}
		return true;
	}
	/**
	 * 
	 * @author : SanghyunLee
	 * @return : TimeContextFactor
	 * @desc   : acp id 로 해당 acp에 적용된 time context를 조회 한다. 
	 * @param id
	 * @return
	 */
	private TimeContextFactor getTimeContext(String id) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put(Const.ACP_ID, id);
		map.put(Const.ACR_TYPE, Const.ACR_TYPE_PV);
		
		TimeContextFactor timeContextFactor = accessControlMapper.retrieveTimeContext(map);
		log.info(" checkTimeContext : {}, expired??? {}", timeContextFactor, timeContextFactor.checkExpired());
		
		//만료가 되었으면 리턴하고 
		if(timeContextFactor.getExpiredYn()) return timeContextFactor;
		
		//만료가 안되었으면 만료가 되었는지 확인 후 리턴한다. 
		if(timeContextFactor.checkExpired()) {
			//update 함
			setTimeExpired(map);
			
		}
		return timeContextFactor;
	}
	
	public List<AccessControl> retreiveAllResources(Long memberSequence, AcpOperationType operationType) {

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(Const.GRANTOR,memberSequence);
		params.put(Const.OPERATION_TYPE,operationType);
//		params.put(Const.ACP_TYPE,AcpType.RESOURCE_GRANTEE);
		
		log.info(" in retreiveAllResources : {}", params);
		List<AccessControl> list = accessControlMapper.retrieveAllResource(params);
		
		return list;
	}
	/**
	 * 
	 * @since  : Jul 20, 2019
	 * @author : SanghyunLee
	 * @return : void
	 * @desc   : 리소스가 업데이트 되는 경우... 다른 리소스의 OWNER가 변경되는 경우가 있을까...(사용자 변경..)=> 리소스의  creator가 바뀌는것만 감지하면 됨
	 * @param memberSequence
	 * @param resourceUri
	 * @param class1
	 */
	public void updateResource(Long memberSequence, String resourceUri, Class<?> class1) {
		log.debug("updateResource : memberSequence {}. resourceUrl : {} , class1 : {}", memberSequence, resourceUri, class1==null?null:class1.getSimpleName());
		//해당 리소스 url/**로 검색하여 연관된 모든 acp 수정 , 
		//acp_type이 "00" 이면 해당 입력 받은 memberSequence로 존재하는 acp를 찾아서 매핑 시킨다 . 다른 것 삭제 
		//acp_type이 "01" 이면 해당 입력 받은 memberSequence로 동일한 acp (type "01")만들어서 매핑 시킨다.
		
		
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Const.RESOURCE_URI, resourceUri);
		//params.put(Const.ACP_TYPE, AcpType.RESOURCE_OWNER.getValue()); //주석 풀면 매핑 풀때 acpType이 00인것만 해제
		params.put(Const.GRANTOR, memberSequence);
		//params.put(Const.OPERATION_TYPE, new ArrayList<>(Arrays.asList(AcpOperationType.ALL.getValue())));//ALL
//		
		
		List<AccessControl> result = accessControlMapper.retrieveAllRelatedResources(params);
		
		log.debug("result : {}", result);//
		log.debug("size of result : {}",result.size());
		
		//조회한 결과가 없으면 리소스 등록 시퀀스를 타고 종료한다.
		if(result == null || result.isEmpty()) {
			log.info("no registered resource -> registerResource memberSequence {}. resourceUrl : {}", memberSequence, resourceUri);
			registerResource(memberSequence, resourceUri, null);
			return;
		}
		
		//조회한 결과를 loop 돌면서 grantor 가 memberSequence와 같으면 pass, 다르면 로직을 타야함
		
		//accessControlMapper.deleteAllRelatedOwnerResources(params); //현재는 00인것만 삭제하는데 01인것도 삭제해야 맞는듯.... why??
		
		for(AccessControl tmp  : result) {
			log.debug("acpDetail of acpId : {}, acpType: {}, detail : {}",tmp.getId(), tmp.getType(), tmp.getAcpDetails());
		
			if(AcpType.RESOURCE_OWNER.equals(tmp.getType()) && 	memberSequence != tmp.getGrantor()) { //resource owner 가 변경됨 !!!!
				log.info("ACP-TYPE : Owner : 00 , grantor : {} to memberSeuqence : {}", tmp.getGrantor(), memberSequence);
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put(Const.ACP_TYPE, AcpType.RESOURCE_OWNER.getValue());
				map.put(Const.GRANTOR, memberSequence);
				
				//acp creator, acp_dtl 의 spv, pv originator 변경
				chaangeAcpOwner(tmp,memberSequence);
				//checkExistingAcpAndMappingResource(accessControlMapper.retrieveUsedAcpWithMember(map), memberSequence, resourceUri);
							
			} else {
				log.info("ACP-TYPE : GRANTEE : 01 ");
				//FIXME !!!권한 부여하는것 추가 후
			}
		}
	}
	
	private void chaangeAcpOwner(AccessControl tmp, Long memberSequence) {
		log.debug("in chaangeResourceOwner : new Owner : {} , acp {}", memberSequence, tmp);
		Long oldGrantor = tmp.getGrantor();
		
		log.debug("before changeAcpOwner : {}", tmp);
		tmp.setGrantor(memberSequence);
		List<Long> originators = getChangedOiriginators(memberSequence,oldGrantor, tmp.getOriginators());
		tmp.setOriginators(originators);
		
		if(tmp.getAcpDetails() != null) {
			List<AcpDetail> acpDetails = tmp.getAcpDetails();
			for(AcpDetail detail : acpDetails) {
				getChangedOiriginators(memberSequence, oldGrantor, detail.getOriginator());
			}
		}
		
		log.debug("after changeAcpOwner : {}", tmp);
		int result  = accessControlMapper.changeAcpOwner(tmp);
		log.debug("result of update Acp : {}", result);
	}

	private List<Long> getChangedOiriginators(Long newGrantor, Long oldGrantor, List<Long> originators) {
		log.debug("getChangedOiriginators  newGrantor : {}, oldGrantor : {}, originators : {},",  newGrantor, oldGrantor, originators);
		if(originators == null) {
			originators = new ArrayList<Long>();
		}
		originators.remove(oldGrantor);
		originators.add(newGrantor);
		
		return originators;
	}
//	private List<Long> getChangedOiriginators(List<Long> originators, Long newGrantor, Long oldGrantor) {
//		log.debug("changeAcpDetailOwner  newGrantor : {}, oldGrantor : {}, detail : {},",  newGrantor, oldGrantor, detail);
//		List<Long> originators = detail.getOriginator();
//		//log.debug(detail.toString());
//		originators.remove(oldGrantor);
//		//log.debug(detail.toString());
//		originators.add(newGrantor);
//		//log.debug(detail.toString());
//		return originators;
//	}

	/**
	 * 해당 사용자의 Master Acp가 없으면 Master Acp를 생성하고, 있으면 해당 Acp에 리소스를 매핑한다.
	 * @author : SanghyunLee
	 * @return : void
	 * @desc   : 
	 * @param existedAcpId
	 * @param memberSequence
	 * @param resourceUri
	 */
	private void checkExistingAcpAndMappingResource(String existedAcpId ,Long memberSequence, String resourceUri) {
		log.debug("checkExistingAcpAndMappingResource ");
		
		log.debug("existedAcpId:{}",existedAcpId);
		//새로 받은 memberSequence로 존재하는 acp를 owner acp를 찾는다
	
		AccessControl acp = null;
		
		if(existedAcpId == null){
			
			//없으면 만들기
			log.info("registerResource - make new acp , memberSequence : {}, resourceUri : {} ", memberSequence, resourceUri);
			acp = makeAccessControl(AcpType.RESOURCE_OWNER, memberSequence, new ArrayList<>(Arrays.asList(memberSequence)), new ArrayList<>(Arrays.asList(AcpOperationType.ALL)),new ArrayList<>(Arrays.asList(resourceUri)), null, null, null);

			if(acp != null && acp.getAcpDetails() != null) {
				log.debug("generated spvDetail :{}",acp.getAcpDetails().get(0));
				log.debug("generated pvDetail :{}",acp.getAcpDetails().get(1));
			}
			log.info("generated acp :{}",acp);
			//insert acp
			accessControlMapper.insertAcp(acp);
		} else {
			acp = new AccessControl();
			acp.setResourceUriList(new ArrayList<>(Arrays.asList(resourceUri)));
			acp.setId(existedAcpId);
		}
		//만들어진 acp를 가지고 리소스 매핑
		accessControlMapper.insertResourceMapping(acp);
	}

	/**
	 * 
	 * @since  : Jul 20, 2019
	 * @author : SanghyunLee
	 * @return : void
	 * @desc   : 리소스가 삭제 되는 경우,  리소스 - 권한 매핑만 삭제하고 acp 자체는 그래도 둔다 (하위 리소스 모두 매핑 삭제)
	 * @param memberSequence
	 * @param resourceUri
	 * @param class1
	 */
	public void deleteResource(Long memberSequence, String resourceUri, Class<?> class1) {

		log.debug("deleteResource : memberSequence {}. resourceUri : {} ", memberSequence, resourceUri);

		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Const.RESOURCE_URI, resourceUri);

		int result = accessControlMapper.deleteAllRelatedResources(params);
		
		log.debug("result of deleteResource : {}", result);
	}

	
	public void timeExpiredSchedule() {
		
		log.debug("in timeExpiredSchedule.... check db expired timeContext");
		
		//mapper를 통해서 조회해서 벌크 업데이트		
		List<TimeContextFactor> list = accessControlMapper.retrieveTimeContextForSchedule();

		log.debug("scheduled : {}",list);
		//FIXME !!!이후 로직 추가 필요 
	}

	public AccessControl retrieveAccessControlPolicy(Long memberSequence, String accessControlId) {
		
		log.debug("retrieveAccessControlPolicy : memberSequence   {}, accessControlId {}", memberSequence, accessControlId);
		
		Map<String, Object> params = new HashMap<String, Object>();

		params.put(Const.MEMBER_SEQUENCE, memberSequence);
		params.put(Const.ACP_ID,accessControlId);
		
		AccessControl accessControl = accessControlMapper.retrieveAccessControlPoliciy(params);
		
		if(accessControl == null) {
			log.warn("no accessControl : {} for {}", accessControl, memberSequence);
			throw new BadRequestException(ResponseCode.INTERNAL_ASSOCIATED_PARAMETER_NOT_FOUND);
		}
		
		if(accessControl.checkUseContext()) {
//			log.info("context type : {}", acp.getContexts());
			//FIXME !!! 장치 권한 부여 개선 후 적용 -- context 여러개 처리 가능하도록
//			switch(acp.getContexts().get(0).getType())
			log.debug("context 조사해서 리턴해야 함  : {}",accessControl.getId());
			
			accessControl.setContexts(Arrays.asList(getTimeContext(accessControl.getId())));
		}
		return accessControl;
	}
	
	public List<AccessControl> retrieveAccessControlPolicies(Long memberSequence, AcpOperationType operationType,
			String resourceUri) {
		log.debug("retrieveAccessControls : memberSequence   {}, operationType {}, resourceUri {}", memberSequence, operationType, resourceUri);
		
		Map<String, Object> params = new HashMap<String, Object>();

		params.put(Const.MEMBER_SEQUENCE, memberSequence);
		params.put(Const.OPERATION_TYPE,operationType);
		params.put(Const.RESOURCE_URI, resourceUri);

		List<AccessControl> list = accessControlMapper.retrieveAccessControlPolicies(params);
		
		for(AccessControl acp : list) {
			
			if(acp.getContextUseYn()) {
//				log.info("context type : {}", acp.getContexts());
				//FIXME !!! 장치 권한 부여 개선 후 적용 -- context 여러개 처리 가능하도록
//				switch(acp.getContexts().get(0).getType())
				log.debug("context 조사해서 리턴해야 함  : {}",acp.getId());
				
				acp.setContexts(Arrays.asList(getTimeContext(acp.getId())));
			}
			
			if(acp.getId() != null && acp.getId().startsWith(Const.GRANT_ACP_ID_PREFIX)) {
				acp.setId(parsingAcpId(acp));
			}
			
		}
		
		if(log.isDebugEnabled()) {
			log.debug("count : {}", list!=null?list.size():"list is empty");
			log.debug("result of retrieve polices : {}", list!=null?list.toString():"list is null");
		}
		
		return list;
	}

	public AccessControl registerAccessControl(Long memberSequence, AccessControl accessControl) {
	
		setGrantorInOriginators(memberSequence, accessControl);
		//FIXME acpType을 입력 받을까??? 
//		AccessControl acpToRegister = makeAccessControl(AcpType.RESOURCE_GRANTEE, memberSequence, accessControl.getOriginators(), accessControl.getOperationType(), 
//				accessControl.getResourceUriList(), accessControl.getContexts(), genGranteeAcpId(memberSequence, accessControl.getId()), accessControl.getName());
		//TODO genGranteeAcpId 생략 그냥 입력 받은 ID로 생성
		AccessControl acpToRegister = makeAccessControl(AcpType.RESOURCE_GRANTEE, memberSequence, accessControl.getOriginators(), accessControl.getOperationType(), 
				accessControl.getResourceUriList(), accessControl.getContexts(), accessControl.getId(), accessControl.getName());
		
		accessControlMapper.insertAcp(acpToRegister);
		
		
		//만들어진 acp를 가지고 리소스 매핑 RESRC_BY_ACP_REL
		accessControlMapper.insertResourceMapping(acpToRegister);
		
		
		if(acpToRegister.getContextUseYn() && acpToRegister.getContexts() !=null) {
			for(ContextFactor factor : acpToRegister.getContexts()) {
				
				if(Const.TIME_CONTEXT_TYPE.equals(factor.getType()) && factor instanceof TimeContextFactor ){
					Map<String, Object> contextParam = new HashMap<String, Object>();
					contextParam.put(Const.ACP_ID,acpToRegister.getId());
					contextParam.put(Const.ACR_TYPE,AcpDetailType.PRIVILEGE.getValue());
					contextParam.put(Const.TIME_CONTEXT_EXPIRES_AT,((TimeContextFactor) factor).getExpiresAt());
					contextParam.put(Const.RESOURCE_URI_LIST,acpToRegister.getResourceUriList());
					accessControlMapper.insertTimeContextFactor(contextParam);
				}
					
			}
		}
		
		return acpToRegister;
		
	}

	private void setGrantorInOriginators(Long memberSequence, AccessControl accessControl) {
		if(accessControl == null || accessControl.getGrantor() == null || memberSequence == null) {
			log.warn("no accessControl or no grantor in request : memberSequence {}, acp : {}", memberSequence, accessControl);
			throw new BadRequestException(ResponseCode.INTERNAL_MANDATORY_PARAMETER_MISSING);
		}
		List<Long> originators = accessControl.getOriginators();
		
		if(originators.contains(memberSequence)) 
			return;
		
		else {
			originators.add(memberSequence);
		}
		log.debug("in setGrantorInOriginators :{}", accessControl.getOriginators());
	}

	private String genGranteeAcpId(Long memberSequence, String id) {
		if(memberSequence == null || id == null) {
			log.warn(" could not generate acp id , memeberSequence : {} , in put id : {}", memberSequence, id);
			throw new BadRequestException(ResponseCode.INTERNAL_MANDATORY_PARAMETER_MISSING);
		}
		StringBuilder builder = new StringBuilder(id).append(Const.ACP_DELIMITER).append(memberSequence);
		return builder.toString();
	}

	public AccessControl getResultFormat(AccessControl result) {
		 AccessControl resultAccessControl = new AccessControl();
		  resultAccessControl.setId(parsingAcpId(result));
		  resultAccessControl.setOperationType(result.getOperationType());
		  resultAccessControl.setResourceUriList(result.getResourceUriList());
		  resultAccessControl.setOriginators(result.getOriginators());
		  resultAccessControl.setGrantor(result.getGrantor());
		  if(result.getContexts() != null && !result.getContexts().isEmpty()) {
			  resultAccessControl.setContexts(result.getContexts());
			  resultAccessControl.setContextUseYn(true);
		  }
		  return resultAccessControl;
	}

	//FIXME 음... 좀더 에러 발생 가능한 코드 수정,....
	private String parsingAcpId(AccessControl result) {

		String parsedId = null;
		if(!result.getId().startsWith(Const.GRANT_ACP_ID_PREFIX)) {
			log.warn("acp id is not startsWith :{} id : {}", Const.GRANT_ACP_ID_PREFIX,result.getId());
			return result.getId();
		}
		parsedId = result.getId().split(Const.GRANT_ACP_ID_PREFIX)[1];
		
		String member = String.valueOf(result.getGrantor());
		parsedId = parsedId.substring(0,parsedId.length()-member.length()-1);
		
		return parsedId;
	}
}
