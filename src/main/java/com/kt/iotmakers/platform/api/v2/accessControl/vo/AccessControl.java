package com.kt.iotmakers.platform.api.v2.accessControl.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.ToString;

@ToString
@JsonInclude(Include.NON_EMPTY)
public @Data class AccessControl {
	
	String Id;
	String name;
	AcpType type;
	Boolean result;
	List<String> resourceUriList;
	List<AcpOperationType> operationType;
	Long grantor;
	List<Long> originators;
	List<AcpDetail> acpDetails;
	List<ContextFactor> contexts;
	Boolean contextUseYn;

	public boolean checkUseContext() {
		if(this.contextUseYn == null || !this.contextUseYn) return false;
		return true;
	}
	
}
