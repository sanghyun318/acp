package com.kt.iotmakers.platform.api.v2.accessControl.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_EMPTY)
public class AcpDetail {
	/*
	* 		[acp_dtl]
	 * 			acp_id
	 * 			acr_type : spv, pv
	 * 			acr_oprs : 00 (all), 01 ()
	 * 			originators : spv {grantor}, pv {originator} 
	 */
	String Id;
	AcpDetailType type;
	List<AcpOperationType> operationType;
	List<Long> originator;
	
	
	/**
	 * @return the id
	 */
	public String getId() {
		return Id;
	}
	/**
	 * @return the type
	 */
	public AcpDetailType getType() {
		return type;
	}
	/**
	 * @return the operationType
	 */
	public List<AcpOperationType> getOperationType() {
		return operationType;
	}
	/**
	 * @return the originator
	 */
	public List<Long> getOriginator() {
		return originator;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		Id = id;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(AcpDetailType type) {
		this.type = type;
	}
	/**
	 * @param operationType the operationType to set
	 */
	public void setOperationType(List<AcpOperationType> operationType) {
		this.operationType = operationType;
	}
	/**
	 * @param originator the originator to set
	 */
	public void setOriginator(List<Long> originator) {
		this.originator = originator;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AcpDetail [Id=" + Id + ", type=" + type + ", operationType=" + operationType + ", originator="
				+ originator + "]";
	}
	
	
}
