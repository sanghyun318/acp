package com.kt.iotmakers.platform.api.v2.accessControl.vo;

import java.util.HashMap;
import java.util.Map;



/**
 *acp에서 타입
 * @author SanghyunLee
 * @since 2019. 5. 9.
 *
 */
public enum AcpDetailType{
	
	SELF_PRIVILEGE("spv"), 
	PRIVILEGE("pv");
	
	private String value ;
	private static final Map<String, AcpDetailType> map = new HashMap<String, AcpDetailType>();
	
	private AcpDetailType(String value) {
		// TODO Auto-generated constructor stub
		this.value = value;
	}
	
	static {
		for (AcpDetailType it : values()) {
			map.put(it.getValue(), it);
		}
	}

	// value에 해당되는 enum을 반환
	public static AcpDetailType getEnum(String value) {
		return map.get(value);
	}
	
	public String getValue(){
		return this.value;
	}

	public  boolean isValue(String value){
		return this.value == value;
	}
	
}
