package com.kt.iotmakers.platform.api.v2.accessControl.vo;

import java.util.HashMap;
import java.util.Map;



/**
 *acp에서 관리하는 operation 종류를 나타냄
 * @author SanghyunLee
 * @since 2019. 5. 8.
 *
 */
public enum AcpOperationType{
	RETRIEVE ("01"), //01
	UPDATE 	 ("02"), //02
	CREATE 	 ("03"), //03
	DELETE 	 ("04"), //04
	GRANT 	 ("05"),//05
	ALL 	 ("00");//00

	private String value ;
	private static final Map<String, AcpOperationType> map = new HashMap<String, AcpOperationType>();
	
	private AcpOperationType(String value) {
		// TODO Auto-generated constructor stub
		this.value = value;
	}
	
	static {
		for (AcpOperationType it : values()) {
			map.put(it.getValue(), it);
		}
	}

	// value에 해당되는 enum을 반환
	public static AcpOperationType getEnum(String value) {
		return map.get(value);
	}
	
	public String getValue(){
		return this.value;
	}

	public  boolean isValue(String value){
		return this.value == value;
	}
	
	public static boolean hasValue(String value) {
		if(map.containsKey(value)) {
			return true;
		}
		else {
			return false;
		}
	}
	
}
