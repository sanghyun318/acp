/**
 * <PRE>
 *  Project 3MP.master-api
 *  Package com.kt.iot.api.v11.device
 * </PRE>
 * @brief
 * @file FlagTypeHandler.java
 * @date 2015. 12. 17. 오후 4:25:48
 * @author kim.seokhun@kt.com
 *  변경이력
 *        이름     : 일자          : 근거자료   : 변경내용
 *       ------------------------------------
 *        kim.seokhun@kt.com  : 2015. 12. 17.       :            : 신규 개발.
 *
 * Copyright © 2013 kt corp. all rights reserved.
 */
package com.kt.iotmakers.platform.api.v2.accessControl.vo;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kt.iotmakers.platform.base.exception.BadRequestException;
import com.kt.iotmakers.platform.base.message.ResponseCode;
import com.kt.iotmakers.platform.base.util.ObjectConverter;

/**
 * <PRE>
 *  ClassName FlagTypeHandler
 * </PRE>
 * @brief
 * @version 1.0
 * @date 2015. 12. 17. 오후 4:25:48
 * @author kim.seokhun@kt.com
 */

public class AcpOperationTypeHandler implements TypeHandler<AcpOperationType> {

	private static Logger logger = LoggerFactory.getLogger(AcpOperationTypeHandler.class);
	/* (non-Javadoc)
	 * @see org.apache.ibatis.type.TypeHandler#setParameter(java.sql.PreparedStatement, int, java.lang.Object, org.apache.ibatis.type.JdbcType)
	 */
	@Override
	public void setParameter(PreparedStatement ps, int i, AcpOperationType parameter, JdbcType jdbcType) throws SQLException {
		logger.debug("in setParameter:{}",parameter!=null?parameter.toString():"is null"); //acp_bas 등록 시 
		if (parameter != null) {
			
			if (AcpOperationType.ALL.equals(parameter)) {
				ps.setString(i, AcpOperationType.ALL.getValue());
			} 
			else if(AcpOperationType.CREATE.equals(parameter)){
				ps.setString(i, AcpOperationType.CREATE.getValue());
			}
			else if(AcpOperationType.RETRIEVE.equals(parameter)){
				ps.setString(i, AcpOperationType.RETRIEVE.getValue());
			}
			else if(AcpOperationType.UPDATE.equals(parameter)){
				ps.setString(i, AcpOperationType.UPDATE.getValue());
			}
			else if(AcpOperationType.DELETE.equals(parameter)){
				ps.setString(i, AcpOperationType.DELETE.getValue());
			}
			else if(AcpOperationType.GRANT.equals(parameter)){
				ps.setString(i, AcpOperationType.GRANT.getValue());
			}
		} else {
			logger.error("");
			throw new BadRequestException(ResponseCode.INTERNAL_INVALID_PARAMETER);
		}
	}

	/* (non-Javadoc)
	 * @see org.apache.ibatis.type.TypeHandler#getResult(java.sql.ResultSet, java.lang.String)
	 */
	@Override
	public AcpOperationType getResult(ResultSet rs, String columnName) throws SQLException {
		String result = rs.getString(columnName);
		logger.info("-------getResult start");
		logger.info("result:{} columnName:{}/",result,columnName);
		logger.info("-------getResult end");
		if (result != null && result.equalsIgnoreCase(AcpOperationType.ALL.getValue())) {
			return AcpOperationType.ALL;
		}
		else if (result.equalsIgnoreCase(AcpOperationType.RETRIEVE.getValue())) {
			return AcpOperationType.RETRIEVE;
		}
		else if (result.equalsIgnoreCase(AcpOperationType.CREATE.getValue())) {
			return AcpOperationType.CREATE;
		}
		else if (result.equalsIgnoreCase(AcpOperationType.UPDATE.getValue())) {
			return AcpOperationType.UPDATE;
		}
		else if (result.equalsIgnoreCase(AcpOperationType.DELETE.getValue())) {
			return AcpOperationType.DELETE;
		}
		else if (result.equalsIgnoreCase(AcpOperationType.GRANT.getValue())) {
			return AcpOperationType.GRANT;
		}
		logger.error("getResult(ResultSet rs, String columnName) no matching result:{}",result);
		return null;
	
	}

	/* (non-Javadoc)
	 * @see org.apache.ibatis.type.TypeHandler#getResult(java.sql.ResultSet, int)
	 */
	@Override
	public AcpOperationType getResult(ResultSet rs, int columnIndex) throws SQLException {
		
		String result = rs.getString(columnIndex);
		
		if (result != null && result.equalsIgnoreCase(AcpOperationType.ALL.getValue())) {
			return AcpOperationType.ALL;
		}
		else if (result.equalsIgnoreCase(AcpOperationType.RETRIEVE.getValue())) {
			return AcpOperationType.RETRIEVE;
		}
		else if (result.equalsIgnoreCase(AcpOperationType.CREATE.getValue())) {
			return AcpOperationType.CREATE;
		}
		else if (result.equalsIgnoreCase(AcpOperationType.UPDATE.getValue())) {
			return AcpOperationType.UPDATE;
		}
		else if (result.equalsIgnoreCase(AcpOperationType.DELETE.getValue())) {
			return AcpOperationType.DELETE;
		}
		else if (result.equalsIgnoreCase(AcpOperationType.GRANT.getValue())) {
			return AcpOperationType.GRANT;
		}
		logger.error("getResult(ResultSet rs, int columnIndex) no matching result:{}",result);
		return null;
	}

	@Override
	public AcpOperationType getResult(CallableStatement cs, int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		String result = cs.getString(columnIndex);
		
		AcpOperationType type = ObjectConverter.toGsonObject(result, AcpOperationType.class);
		return type;
	}

}
