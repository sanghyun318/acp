package com.kt.iotmakers.platform.api.v2.accessControl.vo;

import java.util.HashMap;
import java.util.Map;



/**
 *acp 타입 (리소스 생성으로 인한 ACP 또는 권한 부여를 통해 생성된 ACP)
 * @author SanghyunLee
 * @since 2019. 5. 9.
 *
 */
public enum AcpType{
	
	RESOURCE_OWNER ("00"), 
	RESOURCE_GRANTEE ("01");
	
	private String value ;
	private static final Map<String, AcpType> map = new HashMap<String, AcpType>();
	
	private AcpType(String value) {
		// TODO Auto-generated constructor stub
		this.value = value;
	}
	
	static {
		for (AcpType it : values()) {
			map.put(it.getValue(), it);
		}
	}

	// value에 해당되는 enum을 반환
	public static AcpType getEnum(String value) {
		return map.get(value);
	}
	
	public String getValue(){
		return this.value;
	}

	public  boolean isValue(String value){
		return this.value == value;
	}
	
}
