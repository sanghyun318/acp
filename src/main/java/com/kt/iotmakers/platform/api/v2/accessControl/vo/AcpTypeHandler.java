/**
 * <PRE>
 *  Project 3MP.master-api
 *  Package com.kt.iot.api.v11.device
 * </PRE>
 * @brief
 * @file FlagTypeHandler.java
 * @date 2015. 12. 17. 오후 4:25:48
 * @author kim.seokhun@kt.com
 *  변경이력
 *        이름     : 일자          : 근거자료   : 변경내용
 *       ------------------------------------
 *        kim.seokhun@kt.com  : 2015. 12. 17.       :            : 신규 개발.
 *
 * Copyright © 2013 kt corp. all rights reserved.
 */
package com.kt.iotmakers.platform.api.v2.accessControl.vo;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import com.kt.iotmakers.platform.base.util.ObjectConverter;

/**
 * <PRE>
 *  ClassName FlagTypeHandler
 * </PRE>
 * @brief
 * @version 1.0
 * @date 2015. 12. 17. 오후 4:25:48
 * @author kim.seokhun@kt.com
 */

public class AcpTypeHandler implements TypeHandler<AcpType> {

	/* (non-Javadoc)
	 * @see org.apache.ibatis.type.TypeHandler#setParameter(java.sql.PreparedStatement, int, java.lang.Object, org.apache.ibatis.type.JdbcType)
	 */
	@Override
	public void setParameter(PreparedStatement ps, int i, AcpType parameter, JdbcType jdbcType) throws SQLException {
		 
		if (parameter != null) {
			if (AcpType.RESOURCE_OWNER.equals(parameter)) {
				ps.setString(i, AcpType.RESOURCE_OWNER.getValue());
			} else {
				ps.setString(i, AcpType.RESOURCE_GRANTEE.getValue());
			}
		} else {
			ps.setString(i, AcpType.RESOURCE_OWNER.getValue());
		}
	}

	/* (non-Javadoc)
	 * @see org.apache.ibatis.type.TypeHandler#getResult(java.sql.ResultSet, java.lang.String)
	 */
	@Override
	public AcpType getResult(ResultSet rs, String columnName) throws SQLException {
		String result = rs.getString(columnName);
		
		if (result != null && result.equalsIgnoreCase(AcpType.RESOURCE_OWNER.getValue())) {
			return AcpType.RESOURCE_OWNER;
		}
		return AcpType.RESOURCE_GRANTEE;
	}

	/* (non-Javadoc)
	 * @see org.apache.ibatis.type.TypeHandler#getResult(java.sql.ResultSet, int)
	 */
	@Override
	public AcpType getResult(ResultSet rs, int columnIndex) throws SQLException {
		
		String result = rs.getString(columnIndex);
		
		if (result != null && result.equalsIgnoreCase(AcpType.RESOURCE_OWNER.getValue())) {
			return AcpType.RESOURCE_OWNER;
		}
		return AcpType.RESOURCE_GRANTEE;
	}

	@Override
	public AcpType getResult(CallableStatement cs, int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		String result = cs.getString(columnIndex);
		
		AcpType type = ObjectConverter.toGsonObject(result, AcpType.class);
		return type;

	}

}
