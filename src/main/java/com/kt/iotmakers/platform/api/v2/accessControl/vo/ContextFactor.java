package com.kt.iotmakers.platform.api.v2.accessControl.vo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.kt.iotmakers.platform.Const;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@JsonInclude(Include.NON_EMPTY)
public @Data class ContextFactor {
	
	Long sequence;
	String name; // 사용자가 지정, ID로 사용
	String type; //FIXME enum 생성??
	Date createdOn;
	Date updatedOn;

	
    @JsonCreator
    public static ContextFactor forType(@JsonProperty("type") String type) {
    	
        if(Const.TIME_CONTEXT_TYPE.equals(type)) {
        	TimeContextFactor timeContextFactor = new TimeContextFactor();
        	timeContextFactor.setType(Const.TIME_CONTEXT_TYPE);
        	return timeContextFactor;
        }
		throw new IllegalArgumentException("No matching constant for [" + type + "]");
    }
}
