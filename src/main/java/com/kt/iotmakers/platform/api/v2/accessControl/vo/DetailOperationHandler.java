/**
 * <PRE>
 *  Project 3MP.master-api
 *  Package com.kt.iot.api.v11.device
 * </PRE>
 * @brief
 * @file FlagTypeHandler.java
 * @date 2015. 12. 17. 오후 4:25:48
 * @author kim.seokhun@kt.com
 *  변경이력
 *        이름     : 일자          : 근거자료   : 변경내용
 *       ------------------------------------
 *        kim.seokhun@kt.com  : 2015. 12. 17.       :            : 신규 개발.
 *
 * Copyright © 2013 kt corp. all rights reserved.
 */
package com.kt.iotmakers.platform.api.v2.accessControl.vo;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kt.iotmakers.platform.Const;
import com.kt.iotmakers.platform.base.exception.BadRequestException;
import com.kt.iotmakers.platform.base.message.ResponseCode;
import com.kt.iotmakers.platform.base.util.ObjectConverter;

/**
 * <PRE>
 *  ClassName FlagTypeHandler
 * </PRE>
 * @brief
 * @version 1.0
 * @date 2015. 12. 17. 오후 4:25:48
 * @author kim.seokhun@kt.com
 */

public class DetailOperationHandler implements TypeHandler<List<AcpOperationType>> {

	private static final Logger logger = LoggerFactory.getLogger(DetailOperationHandler.class);
	
	/* (non-Javadoc)
	 * @see org.apache.ibatis.type.TypeHandler#setParameter(java.sql.PreparedStatement, int, java.lang.Object, org.apache.ibatis.type.JdbcType)
	 */
	@Override
	public void setParameter(PreparedStatement ps, int i, List<AcpOperationType> parameter, JdbcType jdbcType) throws SQLException {
		if (parameter != null) {
			
			StringBuilder result = new StringBuilder("{");
			for(AcpOperationType data: parameter){
				result.append(data.getValue());
				result.append(",");
			}
			result.deleteCharAt(result.length()-1);
			result.append("}");
			
			ps.setString(i, result.toString());
			
		}else {
			logger.error("in DetailOperationHandler param is null");
		}
	}

	/* (non-Javadoc)
	 * @see org.apache.ibatis.type.TypeHandler#getResult(java.sql.ResultSet, java.lang.String)
	 */
	@Override
	public List<AcpOperationType> getResult(ResultSet rs, String columnName) throws SQLException {
		String result = rs.getString(columnName);
//		logger.info("DetailOperationHandler 1 :{}",result);
		
		return convertArrayVal(result);
	}

	/* (non-Javadoc)
	 * @see org.apache.ibatis.type.TypeHandler#getResult(java.sql.ResultSet, int)
	 */
	@Override
	public List<AcpOperationType> getResult(ResultSet rs, int columnIndex) throws SQLException {
		
		String result = rs.getString(columnIndex);
		
		if (result != null && result.equalsIgnoreCase(AcpType.RESOURCE_OWNER.getValue())) {
			return null;
		}
		return null;
	}

	@Override
	public List<AcpOperationType> getResult(CallableStatement cs, int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		String result = cs.getString(columnIndex);
		
		AcpType type = ObjectConverter.toGsonObject(result, AcpType.class);
		return null;

	}
	
	private List<AcpOperationType> convertArrayVal(String result) {
		
//		logger.info("in checkArrayVal : {}",result);
		
		if(result != null && result.startsWith("{") && result.endsWith("}")){
			
			String tmp = result.substring(1, result.length() -1);

			String[] splitedStr = splitString(tmp);
			
			if(splitedStr == null) {
				throw new BadRequestException(ResponseCode.INTERNAL_INVALID_PARAMETER);
			}
			
			List<AcpOperationType> operationTypeList = new ArrayList<AcpOperationType>();
			for(String str : splitedStr) {
				
				if(AcpOperationType.hasValue(str)) {
					operationTypeList.add(AcpOperationType.getEnum(str));
				} 
			}
			
			
			return operationTypeList;
		}
		return null;
		//throw new BadRequestException(ResponseCode.INTERNAL_INVALID_PARAMETER);
	}
	
	private String[] splitString(String subString) {
		if(subString != null ) {
			if(subString.contains(Const.ARRAY_DELIMITER)) {
		
				return  subString.split(Const.ARRAY_DELIMITER);
				
			}else {
				String[] result = new String[1];
				result[0] = subString;
				
				return result;
			}
		}
		else {
			logger.error("in splitString fail split : {}", subString);
			return null;
		}
	}

}
