/**
 * <PRE>
 *  Project 3MP.master-api
 *  Package com.kt.iot.api.v11.device
 * </PRE>
 * @brief
 * @file FlagTypeHandler.java
 * @date 2015. 12. 17. 오후 4:25:48
 * @author kim.seokhun@kt.com
 *  변경이력
 *        이름     : 일자          : 근거자료   : 변경내용
 *       ------------------------------------
 *        kim.seokhun@kt.com  : 2015. 12. 17.       :            : 신규 개발.
 *
 * Copyright © 2013 kt corp. all rights reserved.
 */
package com.kt.iotmakers.platform.api.v2.accessControl.vo;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

/**
 * <PRE>
 *  ClassName FlagTypeHandler
 * </PRE>
 * @brief
 * @version 1.0
 * @date 2015. 12. 17. 오후 4:25:48
 * @author kim.seokhun@kt.com
 */

public class FlagTypeNegativeHandler implements TypeHandler<Boolean> {

	/* (non-Javadoc)
	 * @see org.apache.ibatis.type.TypeHandler#setParameter(java.sql.PreparedStatement, int, java.lang.Object, org.apache.ibatis.type.JdbcType)
	 */
	@Override
	public void setParameter(PreparedStatement ps, int i, Boolean parameter, JdbcType jdbcType) throws SQLException {
		if (parameter != null) {
			if (parameter == true) {
				ps.setString(i, "Y");
			} else {
				ps.setString(i, "N");
			}
		} else {
			ps.setString(i, "N");
		}
	}

	/* (non-Javadoc)
	 * @see org.apache.ibatis.type.TypeHandler#getResult(java.sql.ResultSet, java.lang.String)
	 */
	@Override
	public Boolean getResult(ResultSet rs, String columnName) throws SQLException {
		String result = rs.getString(columnName);
		return getResult(result);
	}

	private Boolean getResult(String result) {
		
		if (result == null) return false; 
		if (result.equalsIgnoreCase("Y")) {
			return true;
		} else if (result.equalsIgnoreCase("N")){
			return false;
		} else {
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see org.apache.ibatis.type.TypeHandler#getResult(java.sql.ResultSet, int)
	 */
	@Override
	public Boolean getResult(ResultSet rs, int columnIndex) throws SQLException {
		String result = rs.getString(columnIndex);
		return getResult(result);
	}

	/* (non-Javadoc)
	 * @see org.apache.ibatis.type.TypeHandler#getResult(java.sql.CallableStatement, int)
	 */
	@Override
	public Boolean getResult(CallableStatement cs, int columnIndex) throws SQLException {
		String result = cs.getString(columnIndex);
		return getResult(result);
	}

}
