/**
 * <PRE>
 *  Project 3MP.master-api
 *  Package com.kt.iot.api.v11.device
 * </PRE>
 * @brief
 * @file FlagTypeHandler.java
 * @date 2015. 12. 17. 오후 4:25:48
 * @author kim.seokhun@kt.com
 *  변경이력
 *        이름     : 일자          : 근거자료   : 변경내용
 *       ------------------------------------
 *        kim.seokhun@kt.com  : 2015. 12. 17.       :            : 신규 개발.
 *
 * Copyright © 2013 kt corp. all rights reserved.
 */
package com.kt.iotmakers.platform.api.v2.accessControl.vo;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kt.iotmakers.platform.Const;
import com.kt.iotmakers.platform.base.exception.BadRequestException;
import com.kt.iotmakers.platform.base.message.ResponseCode;

/**
 * <PRE>
 *  ClassName FlagTypeHandler
 * </PRE>
 * @brief
 * @version 1.0
 * @date 2015. 12. 17. 오후 4:25:48
 * @author kim.seokhun@kt.com
 */


public class ResourceUriListHandler implements TypeHandler<List<String>> {

	private static final Logger logger = LoggerFactory.getLogger(ResourceUriListHandler.class);
	/* (non-Javadoc)
	 * @see org.apache.ibatis.type.TypeHandler#setParameter(java.sql.PreparedStatement, int, java.lang.Object, org.apache.ibatis.type.JdbcType)
	 */
	@Override
	public void setParameter(PreparedStatement ps, int i, List<String> parameter, JdbcType jdbcType) throws SQLException {
		if (parameter != null) {
			
			
			StringBuilder result = new StringBuilder("{");
			for(String data:parameter){
				result.append(data);
				result.append(",");
			}
			result.deleteCharAt(result.length()-1);
			result.append("}");
			ps.setString(i, result.toString());
			
		}else {
			logger.error("in OriginatorHandler param is null");
		}
	}

	/* (non-Javadoc)
	 * @see org.apache.ibatis.type.TypeHandler#getResult(java.sql.ResultSet, java.lang.String)
	 */
	@Override
	public List<String> getResult(ResultSet rs, String columnName) throws SQLException {
		String result = rs.getString(columnName);
		logger.info("DetailOriginatorHandler 1 :{}",result);
		
		if(result != null)
			return Arrays.asList(result);
		else return null;
	}

	/* (non-Javadoc)
	 * @see org.apache.ibatis.type.TypeHandler#getResult(java.sql.ResultSet, int)
	 */
	@Override
	public List<String> getResult(ResultSet rs, int columnIndex) throws SQLException {
		
		String result = rs.getString(columnIndex);
		
		
		return null;
	}

	@Override
	public List<String> getResult(CallableStatement cs, int columnIndex) throws SQLException {
		// TODO Auto-generated method stub
		String result = cs.getString(columnIndex);
		
		
		return null;

	}
	
	private List<Long> convertArrayVal(String result) {
		
		logger.info("in checkArrayVal : {}",result);
		
		if(result != null && result.startsWith("{") && result.endsWith("}")){
			
			String tmp = result.substring(1, result.length() -1);

			String[] splitedStr = splitString(tmp);
			
			if(splitedStr == null) {
				throw new BadRequestException(ResponseCode.INTERNAL_INVALID_PARAMETER);
			}
			
			List<Long> originatorList = new ArrayList<Long>();
			for(String str : splitedStr) {
				
					originatorList.add(Long.valueOf(str));
			}
			
			logger.info("originatorList : {}", originatorList.toString());// [ALL]
			return originatorList;
		}
		return null;
		//throw new BadRequestException(ResponseCode.INTERNAL_INVALID_PARAMETER);
	}
	
	private String[] splitString(String subString) {
		if(subString != null ) {
			if(subString.contains(Const.ARRAY_DELIMITER)) {
		
				return  subString.split(Const.ARRAY_DELIMITER);
				
			}else {
				String[] result = new String[1];
				result[0] = subString;
				
				return result;
			}
		}
		else {
			logger.error("in splitString fail split : {}", subString);
			return null;
		}
	}

}
