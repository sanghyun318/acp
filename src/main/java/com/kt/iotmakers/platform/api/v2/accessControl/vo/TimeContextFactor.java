package com.kt.iotmakers.platform.api.v2.accessControl.vo;

import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@ToString(callSuper=true)
@Slf4j
public @Data class TimeContextFactor extends ContextFactor {

	
	Long expiresAt;
	Boolean expiredYn;

	public TimeContextFactor(){
		
	}
	public TimeContextFactor(String type){
		super.setType(type);;
	}
	
	public void setExpiresAtAfter(Long time) {

		expiresAt = System.currentTimeMillis() + time;
		log.info("in setExpiresAtAfter : current : {}, expires_at : {}", System.currentTimeMillis(), expiresAt);
	}
	
	public boolean checkExpired() {
		//expiredAt은 not null
		
		if(Boolean.TRUE.equals(expiredYn)) return true;

		if(System.currentTimeMillis()/1000 > this.expiresAt) return true;
		
		return false;
	}
}
