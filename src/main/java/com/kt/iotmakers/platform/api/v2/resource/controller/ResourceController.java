package com.kt.iotmakers.platform.api.v2.resource.controller;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kt.iotmakers.platform.Const;
import com.kt.iotmakers.platform.Version;
import com.kt.iotmakers.platform.api.v2.accessControl.service.AccesscontrolService;
import com.kt.iotmakers.platform.api.v2.accessControl.vo.AccessControl;
import com.kt.iotmakers.platform.api.v2.accessControl.vo.AcpOperationType;
import com.kt.iotmakers.platform.base.exception.BadRequestException;
import com.kt.iotmakers.platform.base.message.ResponseCode;
import com.kt.iotmakers.platform.base.mvc.message.Messages;
import com.kt.iotmakers.platform.base.util.ResourceParsingUtil;
import com.kt.iotmakers.platform.config.properties.ResourceInfo;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import springfox.documentation.annotations.ApiIgnore;


@Slf4j
@RestController
@RequestMapping("/" + Version.V2+ Const.RESC_API_PATH)
public class ResourceController {

	private static final Logger logger = LoggerFactory.getLogger(ResourceController.class);
	
	
	@Autowired
	AccesscontrolService accessConrtolService;
	@Autowired
	ResourceParsingUtil resourceParsingUtil;
    
	@ApiOperation(value = "동적URL", notes = "사용자의 리소스 접근 권한 확인 (동적URL 설정. ** 는 resourceURI를 뜻한다)", httpMethod = "GET", response = AccessControl.class)
	@RequestMapping(value = "/**", method = GET)
	private AccessControl checkAccesscontrolForResource (
			@ApiParam(name="token", value="token", required=false)
	 	 	@RequestHeader(value="Authorization") String token,
	 	 	@ApiParam(name="tokenType", value="tokenType : admin or null", required=false)
	 	 	@RequestHeader(value="tokenType", required=false) String tokenType,
	 	 	@ApiParam(name="memberSequence", value="권한을 부여할 사용자", required=false)
	 	 	@RequestParam (required=false) Long extrMemberSequence,
			@ApiParam(name="operationType", value="operationType", required=false) @RequestParam(required=false) AcpOperationType operationType,
			HttpServletRequest request,
			@ApiIgnore Messages messages) throws Exception {
		
		logger.debug(" 리소스 목록 조회" );
		Long memberSequence = accessConrtolService.getMemberFromToken(token, tokenType, extrMemberSequence);
		
		log.info("토큰 정보 확인 token : {}, tokenType : {},  extrMember :{}, memberSequence : {}", token, tokenType,  extrMemberSequence, memberSequence);
		
		if(operationType == null) {
			logger.error("operationType is missing");
			throw new BadRequestException(ResponseCode.INTERNAL_MANDATORY_PARAMETER_MISSING);
		}
		if(memberSequence == null) {
			//invalid memberSequence -> error 리턴
			throw new BadRequestException(ResponseCode.INTERNAL_INVALID_PARAMETER);
		}
		logger.debug("memberSequence : {}",memberSequence);
		
		//check operationType
		logger.debug("operationType : {}",operationType.getValue());

		//getResourceUrl from HttpServletRequet
		String resourceUrl = resourceParsingUtil.getResourceUrl(request);
		
		//parsingResourceUrl
		ResourceInfo info = resourceParsingUtil.parsingResourceUrl(resourceUrl);
		
		if(info == null) {
			//없는 url로 요청 -> error 리턴 
			throw new BadRequestException(ResponseCode.INTERNAL_INVALID_PARAMETER);
		}
		logger.debug("info >>> {}",info.toString());
		logger.debug("resource info in controller {}", info.toString());
		
		AccessControl result = accessConrtolService.checkResourceWithInfoAndRetry(resourceUrl, info, memberSequence, operationType);
		result.setResourceUriList(new ArrayList<String>(Arrays.asList(resourceUrl)));
		
		messages.addMessage(Const.RESPONSE_CODE_OK, "");
	    return result;
	}
	
	@Deprecated
	@ApiOperation(value = "리소스 접근 권한 부여", notes = "리소스 접근 권한을 부여합니다.", httpMethod = "POST")
    @RequestMapping(value="/grant", method=POST)
	private AccessControl grantAccessControl(
							 	 	@ApiParam(name="token", value="token", required=false)
							 	 	@RequestHeader(value="Authorization") String token,
							 	 	@ApiParam(name="tokenType", value="tokenType : admin or null", required=false)
							 	 	@RequestHeader(value="tokenType", required=false) String tokenType,
							 	 	@ApiParam(name="memberSequence", value="권한을 부여할 사용자", required=false)
							 	 	@RequestParam (required=false) Long extrMemberSequence,
							 	 	@ApiParam(name="acp", value="권한 정보", required=true)
							 	 	@RequestBody (required=false) AccessControl accessControl,
							 	 	
							 	 	@ApiIgnore
 	 								Messages messages) {

			
	  logger.info("입력 받은 권한 정보 확인... acp : {},",accessControl!=null?accessControl.toString():"accessContro is null"); 
	  Long memberSequence = accessConrtolService.getMemberFromToken(token,tokenType,extrMemberSequence);
	 
	  
	  accessConrtolService.validAccessControl(accessControl);
	  
	  //parsingResourceUrl
	  ResourceInfo info = resourceParsingUtil.parsingResourceUrl(accessControl.getResourceUriList().get(0));
			
			if(info == null) {
				//없는 url로 요청 -> error 리턴 
				throw new BadRequestException(ResponseCode.INTERNAL_INVALID_PARAMETER);
			}		
	  accessConrtolService.checkAccessControlOfResourceList(accessControl.getResourceUriList(),memberSequence, AcpOperationType.GRANT,info);
	 
	  
	/**
	 * 권한을 받을 사람이 해당 acp가 이미 있는지 확인함
	 * acp가 없으면 acp를 생성
	 * 리소스와 acp를 매핑한다.
	 */
	  accessConrtolService.grantAccessControl(memberSequence,accessControl.getOperationType(),accessControl.getOriginators(),accessControl.getResourceUriList(), accessControl.getContexts());
	  
	  
	  AccessControl resultAccessControl = new AccessControl();
	  resultAccessControl.setOperationType(accessControl.getOperationType());
	  resultAccessControl.setResourceUriList(accessControl.getResourceUriList());
	  resultAccessControl.setOriginators(accessControl.getOriginators());
	  resultAccessControl.setGrantor(memberSequence);
	  if(accessControl.getContexts() != null && !accessControl.getContexts().isEmpty()) {
		  resultAccessControl.setContexts(accessControl.getContexts());
		  resultAccessControl.setContextUseYn(true);
	  }

	  messages.addMessage(Const.RESPONSE_CODE_OK, "");
	  return resultAccessControl;
	  //return null;
	}

	@Deprecated
	@ApiOperation(value = "리소스 접근 권한 삭제", notes = "리소스 접근 권한을 삭제합니다.", httpMethod = "DELETE")
    @RequestMapping(value="/grant/{acpId}", method=DELETE)
	private void deleteGrantedDeviceAccessControl(
							 	 	@ApiParam(name="token", value="token", required=false)
							 	 	@RequestHeader(value="Authorization") String token,
							 	 	@ApiParam(name="tokenType", value="tokenType : admin or null", required=false)
							 	 	@RequestHeader(value="tokenType", required=false) String tokenType,
							 	 	@ApiParam(name="memberSequence", value="권한을 부여할 사용자", required=false)
							 	 	@RequestParam (required=false) Long extrMemberSequence,
							 	 	@ApiParam(name="acpId", value="삭제 할 acp id", required=true)
							 	 	@PathVariable String acpId,
							 	 	@ApiParam(name="acp", value="권한 정보", required=true)
							 	 	@RequestBody (required=false) AccessControl accessControl,

							 	 	@ApiIgnore
 	 								Messages messages) {

			
	  logger.info("입력 받은 권한 정보 확인... acp : {},", accessControl!=null?accessControl.toString():"accessContro is null"); 
	  logger.info("입력 받은 권한 정보 확인... acp id  : {},", acpId); 
	  Long memberSequence = accessConrtolService.getMemberFromToken(token,tokenType,extrMemberSequence);
	 
	  accessConrtolService.validAccessControl(accessControl);
	  
	//parsingResourceUrl
		ResourceInfo info = resourceParsingUtil.parsingResourceUrl(accessControl.getResourceUriList().get(0));
			
			if(info == null) {
				//없는 url로 요청 -> error 리턴 
				throw new BadRequestException(ResponseCode.INTERNAL_INVALID_PARAMETER);
			}
	  
	  //요청한 사용자가 해당 리소스 삭제 권한 있나 확인
	  accessConrtolService.checkAccessControlOfResourceList(accessControl.getResourceUriList(),memberSequence, AcpOperationType.DELETE, info);
	  accessControl.setId(acpId);
	  accessConrtolService.deleteAccessControl(accessControl, memberSequence);
	  
	  messages.addMessage(Const.RESPONSE_CODE_OK, "");

	}
	
	@Deprecated
	@ApiOperation(value = "리소스 접근 권한 수정", notes = "리소스 접근 권한을 수정합니다.", httpMethod = "PUT")
    @RequestMapping(value="/grant/{acpId}", method=PUT)
	private void updateGrantedDeviceAccessControl(
							 	 	@ApiParam(name="token", value="token", required=false)
							 	 	@RequestHeader(value="Authorization") String token,
							 	 	@ApiParam(name="tokenType", value="tokenType : admin or null", required=false)
							 	 	@RequestHeader(value="tokenType", required=false) String tokenType,
							 	 	@ApiParam(name="memberSequence", value="권한을 부여할 사용자", required=false)
							 	 	@RequestParam (required=false) Long extrMemberSequence,
							 	 	@ApiParam(name="acpId", value="삭제 할 acp id", required=true)
							 	 	@PathVariable String acpId,
							 	 	@ApiParam(name="acp", value="권한 정보", required=true)
							 	 	@RequestBody (required=false) AccessControl accessControl,
							 	 	@ApiIgnore
 	 								Messages messages) {

			
	  logger.info("입력 받은 권한 정보 확인... acp : {},", accessControl!=null?accessControl.toString():"accessContro is null"); 
	  logger.info("입력 받은 권한 정보 확인... acp id  : {},", acpId); 
	  Long memberSequence = accessConrtolService.getMemberFromToken(token,tokenType,extrMemberSequence);
	 
	  accessConrtolService.validAccessControl(accessControl);
	  
	//parsingResourceUrl
		ResourceInfo info = resourceParsingUtil.parsingResourceUrl(accessControl.getResourceUriList().get(0));
			
			if(info == null) {
				//없는 url로 요청 -> error 리턴 
				throw new BadRequestException(ResponseCode.INTERNAL_INVALID_PARAMETER);
			}
	  
	  //요청한 사용자가 해당 리소스 삭제 권한 있나 확인
	  accessConrtolService.checkAccessControlOfResourceList(accessControl.getResourceUriList(),memberSequence, AcpOperationType.UPDATE, info);
	  accessControl.setId(acpId);
	  //FIXME update 위해서 새 request body 객체를 만들어야 한다.
	  //accessConrtolService.retrieveAndDeleteAccessControl(accessControl, memberSequence);
	  
	  messages.addMessage(Const.RESPONSE_CODE_OK, "");

	}
	
	@Deprecated
	@ApiOperation(value = "리소스 접근 권한 조회", notes = "내가 부여한 리소스 접근 권한을 확인합니다", httpMethod = "GET")
    @RequestMapping(value="/grant", method=GET)
	private List<AccessControl> retrieveGrantedDeviceAccessControl(
									@ApiParam(name="token", value="token", required=false)
							 	 	@RequestHeader(value="Authorization") String token,
							 	 	@ApiParam(name="tokenType", value="tokenType : admin or null", required=false)
							 	 	@RequestHeader(value="tokenType", required=false) String tokenType,
							 	 	@ApiParam(name="memberSequence", value="권한을 부여할 사용자", required=false)
							 	 	@RequestParam (required=false) Long extrMemberSequence,
							 	 	@ApiParam(name="operationType", value="operationType", required=false) 
									@RequestParam(required=false) AcpOperationType operationType,
							 	 	@ApiIgnore
 	 								Messages messages) {

			
	  logger.info("내가 접근 권한준거 조회" );
	  
	  Long memberSequence = accessConrtolService.getMemberFromToken(token,tokenType,extrMemberSequence);
	  
	  messages.addMessage(Const.RESPONSE_CODE_OK, "");
	  
	  return accessConrtolService.retrieveGrantedAccessControl(memberSequence, operationType);
	  
	}
	
	@Deprecated
	@ApiOperation(value = "접근 가능한 리소스 목록 조회", notes = "내가 접근 가능한 리소스 목록을 조회합니다.", httpMethod = "GET")
    @RequestMapping(value="", method=GET)
	private List<AccessControl> retrievAllResources(
									@ApiParam(name="token", value="token", required=false)
							 	 	@RequestHeader(value="Authorization") String token,
							 	 	@ApiParam(name="tokenType", value="tokenType : admin or null", required=false)
							 	 	@RequestHeader(value="tokenType", required=false) String tokenType,
							 	 	@ApiParam(name="memberSequence", value="권한을 부여할 사용자", required=false)
							 	 	@RequestParam (required=false) Long extrMemberSequence,
							 	 	@ApiParam(name="operationType", value="operationType", required=false) 
									@RequestParam(required=false) AcpOperationType operationType,
							 	 	@ApiIgnore
 	 								Messages messages) {

			
	  logger.info(" 내가 접근 가능한 리소스 목록을 조회합니다" );
	   
	  Long memberSequence = accessConrtolService.getMemberFromToken(token,tokenType,extrMemberSequence);
	  
	  messages.addMessage(Const.RESPONSE_CODE_OK, "");
	  
	  return accessConrtolService.retreiveAllResources(memberSequence, operationType);
	  
	}
	
	
}
