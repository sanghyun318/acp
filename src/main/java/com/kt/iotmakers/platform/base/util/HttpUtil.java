package com.kt.iotmakers.platform.base.util;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.RequestEntity.HeadersBuilder;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.kt.iotmakers.platform.api.v2.accessControl.vo.AccessControl;
import com.kt.iotmakers.platform.base.message.BaseResponse;


@Service
public class HttpUtil {

	private static final Logger logger = LoggerFactory.getLogger(HttpUtil.class);

//	@Value("#{'${extr.url}'.split(',')}")
	//TODO 임시로 randomUrl위해 처리.......
	private List<String> extrUrl = Arrays.asList("");

	
	private static int roundRobinCount = 0;

	private int getRoundRobinCount(int size) {
		if (roundRobinCount >= size) {
			roundRobinCount = 0;
		}
		return roundRobinCount++;
	}
//	private String getRandomUrl(List<String> url, String contextPath) {
//		if (url != null && url.size() > 0) {
//			return url.get(getRoundRobinCount(extrUrl.size()) - 1) + contextPath;
//		}
//		return null;
//	}
//
//	private URI getRandomUri(List<String> url, String contextPath, String path) {
//		return getUri(getRandomUrl(url, contextPath), path);
//	}
//
//	private URI getRandomUri(List<String> url, String contextPath, String path, Map<String, String> param) {
//		return getUri(getRandomUrl(url, contextPath), path, param);
//	}

	public URI getUri(String baseUrl, String path) {
		return getUri(baseUrl, path, null);
	}

	public URI getUri(String baseUrl, String path, Map<String, String> param) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(baseUrl).path(path);
		
		if (param != null) {
			Iterator<String> iteratortor = param.keySet().iterator();
			while (iteratortor.hasNext()) {
				String key = (String) iteratortor.next();
				builder.queryParam(key, param.get(key));
			}
		}
		URI uri = builder.build().toUri();
		
		return uri;
	}

	protected <T> HttpEntity<T> getRequestEntity(T body) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		return new HttpEntity<T>(body, headers);
	}

	private void setHeaders(HeadersBuilder headersBuilder,  Map<String, Object> headerParam) {
		
		if(headersBuilder == null || headerParam == null) {
			logger.info("headersBuilder or headerParam is null");
			return ;
		}
		
		Iterator<String> keyIterator = headerParam.keySet().iterator();
		
		while(keyIterator.hasNext()) {
			String key = keyIterator.next();
			logger.info("setHeaders key :{}, val : {}", key, headerParam.get(key));
			headersBuilder.header(key, (String) headerParam.get(key));
		}
		
	}
	
	public RequestEntity<BaseResponse<AccessControl>> genRequestEntity(HttpMethod method, URI url, Map<String, Object> bodyParam, Map<String, Object> headerParam){
		
		HeadersBuilder headerBuilder = null;
		switch(method.name()) {
		
		case "GET":
			logger.info("in get of genRequestEntity");
			headerBuilder = RequestEntity.get(url);
			setHeaders(headerBuilder, headerParam);
			return headerBuilder.build();
			
//		case "POST":
//			headerBuilder = RequestEntity.post(url);
//			setHeaders(headersBuilder, headerParam);
		}
		return null;
		
	}
	
	public <T> ResponseEntity<BaseResponse<T>> sendRequest(RequestEntity<BaseResponse<T>> requestEntity, ParameterizedTypeReference<BaseResponse<T>> type) {
		int socketTimeOutCNT = 0;
		try {
		
			return getRestTemplate().exchange(requestEntity, type);

		} catch (ResourceAccessException e) {
			
			logger.error(e.getMessage(), e); // Read timed out 여기 걸린다.
			if (e.getCause() instanceof SocketTimeoutException)
				socketTimeOutCNT++;

			try {
				return getRestTemplate().exchange(requestEntity, type);
				
			} catch (ResourceAccessException e1) {

				logger.error(e1.getMessage(), e1);

				if (e1.getCause() instanceof SocketTimeoutException)
					socketTimeOutCNT++;
				
				if (socketTimeOutCNT > 1)
					return null;

				return getRestTemplate().exchange(requestEntity, type);
			}
		}
	}
	
	//@LoadBalanced
	private RestTemplate getRestTemplate() {
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
		factory.setConnectTimeout(5 * 1000);
		factory.setReadTimeout(10 * 1000);
		RestTemplate restTemplate = new RestTemplate(factory);
		restTemplate.setErrorHandler(new CommonErrorHandler());
		restTemplate.getMessageConverters().add(new GsonHttpMessageConverter());
		return restTemplate;
	}

	public class CommonErrorHandler implements ResponseErrorHandler {

		@Override
		public void handleError(ClientHttpResponse response) throws IOException {
			// your error handling here

		}

		@Override
		public boolean hasError(ClientHttpResponse response) throws IOException {

			return true;
		}
	}
}
