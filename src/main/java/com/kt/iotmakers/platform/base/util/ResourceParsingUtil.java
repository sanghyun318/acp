package com.kt.iotmakers.platform.base.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import com.kt.iotmakers.platform.Version;
import com.kt.iotmakers.platform.base.exception.BadRequestException;
import com.kt.iotmakers.platform.base.message.ResponseCode;
import com.kt.iotmakers.platform.config.properties.AccesControlMeta;
import com.kt.iotmakers.platform.config.properties.ResourceInfo;

@Component
public class ResourceParsingUtil {

	private static final Logger logger = LoggerFactory.getLogger(ResourceParsingUtil.class);
	
	@Autowired
	AccesControlMeta accessControlMeta;
	
	private static final String VALID_RESOURCE_PATH 	=  "/"+Version.V2+"/resources";

	private static  Pattern upperPattern  = null;
	private static  Pattern mainPattern  = null;
	private static  Pattern lowerPattern  = null;
	
	@PostConstruct
	private void settingPatterns() {
	//bean 생성 시 yml에서 속성들을 읽어와 셋팅한다	
//		logger.debug("------------upper : {}",accessControlMeta.getUpperResource().toString());
//		logger.debug("------------main : {}",accessControlMeta.getMainResource().toString());
//		logger.debug("------------lower : {}",accessControlMeta.getLowerResource().toString());
		
		if(accessControlMeta.getUpperResource() != null && accessControlMeta.getUpperResource().getPattern() != null)
			upperPattern = Pattern.compile(accessControlMeta.getUpperResource().getPattern());
		if(accessControlMeta.getMainResource() != null && accessControlMeta.getMainResource().getPattern() != null)
			mainPattern = Pattern.compile(accessControlMeta.getMainResource().getPattern());
		if(accessControlMeta.getLowerResource() != null && accessControlMeta.getLowerResource().getPattern() != null)
			lowerPattern = Pattern.compile(accessControlMeta.getLowerResource().getPattern());
	}
	
	public String getResourceUrl(HttpServletRequest request) {
	
		String contextPath = request.getContextPath();
		String resourceUrl = null;
		
		String tmp = (String) request.getRequestURI(); // /accessControl/v2/resources/**
		logger.debug("before tmp {}",tmp);
		StringBuilder parsingBuiler = new StringBuilder(contextPath).append(VALID_RESOURCE_PATH);
		logger.debug("parsingBuilder: {}",parsingBuiler.toString());
		
		String arr[] = tmp.split(parsingBuiler.toString());
		if(arr != null && arr.length< 2) {
			//문제가 있음 에러 리턴해야 함 ....
			logger.error("no url left {}",tmp);
			BadRequestException exception =new BadRequestException(ResponseCode.INTERNAL_INVALID_PARAMETER);
			throw exception;
			
		}else {
			resourceUrl = arr[1];
			return resourceUrl;
		}
		
	}
	/**
	 * 
	 * @since  : Jul 4, 2019
	 * @author : SanghyunLee
	 * @return : ResourceInfo
	 * @desc   : HttpServletRequest의 request url를 파싱하여 yml에 있는 리소스가 맞는지 확인하여 null을 리턴하거나 해당 리소스 정보를 리턴한다.
	 * @param request
	 * @return
	 */
	public  ResourceInfo parsingResourceUrl(HttpServletRequest request) {
		
		String resourceUrl = getResourceUrl(request);
	
		logger.debug("---------------------resourceUrl :{}",resourceUrl);
		
		ResourceInfo info = findMatchedResourceInfo(resourceUrl, upperPattern, accessControlMeta.getUpperResource());
		if(info != null) return info;

		info = findMatchedResourceInfo(resourceUrl, mainPattern, accessControlMeta.getMainResource());
		if(info != null) return info;
		
		info = findMatchedResourceInfo(resourceUrl, lowerPattern, accessControlMeta.getLowerResource());
		if(info != null) return info;

		logger.debug("-----------------------");
		logger.debug("no resource matched ....");
		return null;
	}
	public  ResourceInfo parsingResourceUrl(String  resourceUrl) {
	
	
		logger.debug("---------------------resourceUrl :{}",resourceUrl);
		
		ResourceInfo info = findMatchedResourceInfo(resourceUrl, upperPattern, accessControlMeta.getUpperResource());
		if(info != null) return info;

		info = findMatchedResourceInfo(resourceUrl, mainPattern, accessControlMeta.getMainResource());
		if(info != null) return info;
		
		info = findMatchedResourceInfo(resourceUrl, lowerPattern, accessControlMeta.getLowerResource());
		if(info != null) return info;

		logger.debug("-----------------------");
		logger.debug("no resource matched ....");
		return null;
	}
	/**
	 * 
	 * @since Jun 21, 2019
	 * @author SanghyunLee
	 * @param resourceUrl
	 * @param pattern
	 * @param resoruceInfo
	 * @return
	 */
	private ResourceInfo findMatchedResourceInfo(String resourceUrl, Pattern pattern, ResourceInfo resoruceInfo) {
		
		if(pattern != null) {
//			if(pattern != null && resoruceInfo !=null) { // resourceInfo에 대한 null 체크는 controller에서
			
			if(findMatchedPattern(resourceUrl, pattern)) {
				logger.debug("matcthed pattern : {}",resoruceInfo!=null?resoruceInfo.toString():"resource is null");
				return resoruceInfo;
			}else {
				logger.debug("not matcthed pattern : {}",resoruceInfo!=null?resoruceInfo.toString():"resource is null");
				return null;
			}
		}else {
			logger.debug("pattern is null or resource info is null");
			return null;
		}
	}
	
	private boolean findMatchedPattern(String resourceUrl, Pattern pattern) {
		if(pattern != null) {
		
			Matcher matcher = pattern.matcher(resourceUrl);
			return matcher.find();
		
		} else {
			return false;
		}
	}
	
	public boolean validResourceUrl(String resourceUrl) {
		// TODO Auto-generated method stub
		if(!findMatchedPattern(resourceUrl, upperPattern))
			if(!findMatchedPattern(resourceUrl, mainPattern))
				return findMatchedPattern(resourceUrl, lowerPattern);
			else return true;
		else return true;
	}

	public ResourceInfo getResourceInfo(String resourceUrl) {
		// TODO Auto-generated method stub
		if(findMatchedPattern(resourceUrl,upperPattern))
			return accessControlMeta.getUpperResource();
		else if(findMatchedPattern(resourceUrl, mainPattern))
			return accessControlMeta.getMainResource();
		else if (findMatchedPattern(resourceUrl, lowerPattern))
			return accessControlMeta.getLowerResource();
		else return null;
	}
	
	public String parsingUrlToUpperResource(String resourceUrl, ResourceInfo info) {
		logger.info("in parsingUrlToUpperResource : {}, {}",resourceUrl, info.toString());
		Pattern parsingPattern = Pattern.compile(info.getParsing());
		String[] str= parsingPattern.split(resourceUrl);
		if(str != null) {
			
				return str[0];
		}
		 else {
			return null;
		}
	}

	public ResourceInfo getUpperResourceInfo(ResourceInfo info) {
		// TODO Auto-generated method stub
		switch(info.getName()) {
		
		case "main":
			return accessControlMeta.getUpperResource();
		case "lower":
			return accessControlMeta.getMainResource();
		}
		return null;
	}
}
