package com.kt.iotmakers.platform.config;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.kt.iotmakers.platform.Env;

import net.sf.log4jdbc.Log4jdbcProxyDataSource;
import net.sf.log4jdbc.tools.Log4JdbcCustomFormatter;
import net.sf.log4jdbc.tools.LoggingType;

@Configuration
@EnableTransactionManagement
@MapperScan(basePackages = {"**.mapper.**"})
public class DataSourceConfiguration {

	@Value("${jdbc.driverClassName:net.sf.log4jdbc.DriverSpy}")
    private String driverClassName;

    @Value("${jdbc.url:jdbc:log4jdbc:postgresql://localhost:5432/psmcdb}")
    private String jdbcUrl;

    @Value("${jdbc.username:openp}")
    private String jdbcUserName;

    @Value("${jdbc.password:openp}")
    private String jdbcPassword;

    @Value("${dbcp.max.total:8}")
    private int maxTotal;
    @Value("${dbcp.max.idle:8}")
    private int maxIdle;

    @Autowired
    DataSource defaultDataSource;
    
    @Bean(name = "defaultDataSource")
    public DataSource dataSource() {
    	 	
    	//handleDataSourceParam();
    	
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(driverClassName);
        dataSource.setUrl(jdbcUrl);
        dataSource.setUsername(jdbcUserName);
        dataSource.setPassword(jdbcPassword);
        dataSource.setValidationQuery("SELECT 1");
        dataSource.setTestOnBorrow(true);
        dataSource.setTestOnReturn(false);
        dataSource.setTestWhileIdle(true);
        dataSource.setTimeBetweenEvictionRunsMillis(1800000);
        dataSource.setMaxTotal(maxTotal);
        dataSource.setMaxIdle(maxIdle);
        
        return dataSource;
    }

//    @Bean(name = "dataSource")
    @Bean
    public Log4jdbcProxyDataSource dataSourceFormatting() {
    	
    	Log4JdbcCustomFormatter formatter = new Log4JdbcCustomFormatter();
    	formatter.setLoggingType(LoggingType.MULTI_LINE);
    	formatter.setSqlPrefix("[SQL  :   ");   
    	Log4jdbcProxyDataSource afterDataSource =  new Log4jdbcProxyDataSource(defaultDataSource);
    	
    	afterDataSource.setLogFormatter(formatter);
    	return afterDataSource;
    	
    }
    private void handleDataSourceParam() {
		// TODO Auto-generated method stub
    	String host = System.getenv(Env.DB_HOST);
    	String port = System.getenv(Env.DB_PORT);
    	String dbName = System.getenv(Env.DB_NAME);
    	
    	if(host != null && port != null && dbName != null){
    		
    		StringBuilder jdbcUrlBuiler = new StringBuilder("jdbc:log4jdbc:postgresql://");
    		jdbcUrlBuiler.append(host);
    		jdbcUrlBuiler.append(":");
    		jdbcUrlBuiler.append(port);
    		jdbcUrlBuiler.append("/");
    		jdbcUrlBuiler.append(dbName);
    		jdbcUrl =  jdbcUrlBuiler.toString();
    	}
    	
    	jdbcUserName = System.getenv(Env.DB_USER);
    	jdbcPassword = System.getenv(Env.DB_USER_PWD);

    }

	@Bean
    public DataSourceTransactionManager transactionManager() {
//    	return new DataSourceTransactionManager(dataSource());
    	return new DataSourceTransactionManager(dataSourceFormatting());
    }

   
	@Bean
	public SqlSessionFactory sqlSessionFactory(Log4jdbcProxyDataSource dataSource) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(dataSource);
        bean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mapper/*.xml"));
        bean.setConfigLocation(new DefaultResourceLoader().getResource("classpath:mybatis.xml"));

        return bean.getObject();
    }

    @Bean
    public SqlSessionTemplate sqlSession(SqlSessionFactory sqlSessionFactory) {
        return new SqlSessionTemplate(sqlSessionFactory);
    }
}
