package com.kt.iotmakers.platform.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.kt.iotmakers.platform.config.properties.EnvType;
import com.kt.iotmakers.platform.config.properties.PlatformType;
import com.kt.iotmakers.platform.config.properties.ResourceInfo;

@Configuration
public class PropertyConfig {
	

	@Component
	@ConfigurationProperties(prefix = "resource")
	public class ResourceProperty {

		
		private List<ResourceInfo> info; 

		/**
		 * @return the info
		 */
		public List<ResourceInfo> getInfo() {
			return info;
		}

		/**
		 * @param info the info to set
		 */
		public void setInfo(List<ResourceInfo> info) {
			this.info = info;
		}

		@Override
		public String toString() {
			return "ResourceProperty [info=" + info + "]";
		}
	}

	
	@Component
	@ConfigurationProperties(prefix = "env")
	public class CommonPropertyConfig {

		private String type;
		private String platform;
		
		/**
		 * @param type the type to set
		 */
		public void setType(String type) {
			this.type = type;
		}
		public EnvType getEnvType(){
			
			return EnvType.getEnvTypeFromString(type);
		}
		/**
		 * @return the platformEnv
		 */
		public PlatformType getPlatform() {
			return PlatformType.getEnvTypeFromString(platform);
		}
		/**
		 * @param platformEnv the platformEnv to set
		 */
		public void setPlatform(String platformEnv) {
			this.platform = platformEnv;
		}
	}
}
