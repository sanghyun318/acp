package com.kt.iotmakers.platform.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.kt.iotmakers.platform.api.v2.accessControl.service.AccesscontrolService;

@Configuration
@EnableScheduling
public class ScheduleConfig {

	@Autowired
	private AccesscontrolService accesscontrolService;
	
	@Bean
	@ConditionalOnExpression(value = "'${expired.schedule.use:false}'=='true'")
	public ExpiredSchedule getExpiredSchedule() {
		
		return new ExpiredSchedule();
	}
	
	class ExpiredSchedule {
	
		@Scheduled(fixedDelay = 1000*10)
		public void expiredSchedule() {
			accesscontrolService.timeExpiredSchedule();
			
		}
	}
	
}
