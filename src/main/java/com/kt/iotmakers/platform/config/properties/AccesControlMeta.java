package com.kt.iotmakers.platform.config.properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.stereotype.Component;

import com.kt.iotmakers.platform.config.PropertyConfig.CommonPropertyConfig;
import com.kt.iotmakers.platform.config.PropertyConfig.ResourceProperty;

@Component
@AutoConfigureAfter(ResourceProperty.class)
public class AccesControlMeta implements InitializingBean {

	private static final Logger logger = LoggerFactory.getLogger(AccesControlMeta.class);
	
	@Autowired
	ResourceProperty resourceProperties;
	@Autowired
	CommonPropertyConfig commonPropertyConfig;
	
	private ResourceInfo upperResource;
	private	ResourceInfo mainResource;
	private	ResourceInfo lowerResource;
		
	private	EnvType envType;
		
	private static final String upperType = "upper";
	private static final String mainType = "main";
	private static final String lowerType = "lower";

	/**
	 * @return the upperResource
	 */
	public ResourceInfo getUpperResource() {
		return upperResource;
	}

	/**
	 * @return the mainResource
	 */
	public ResourceInfo getMainResource() {
		return mainResource;
	}

	/**
	 * @return the lowerResource
	 */
	public ResourceInfo getLowerResource() {
		return lowerResource;
	}
	
	/**
	 * @return the envType
	 */
	public EnvType getEnvType() {
		return envType;
	}
	
	
	@Override
	public void afterPropertiesSet() throws Exception {
		
		for(ResourceInfo info : resourceProperties.getInfo()) {
			logger.info(info.toString());
			switch (info.getName()) {
			case upperType:
				upperResource = info;
				break;
			case mainType:
				mainResource = info;
				break;
			case lowerType:
				lowerResource = info;
				break;

			default:
				// TODO collect all error message
				logger.error("invalid name, name shoud be one of these : upper, main, lower");
				break;
			}
		}
		envType = commonPropertyConfig.getEnvType();
	}

	

}

