package com.kt.iotmakers.platform.config.properties;

import java.util.ArrayList;
import java.util.List;

import com.kt.iotmakers.platform.api.message.vo.TopicInfo;

public class Consumer {

	String address;
	List<TopicInfo> topics;
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @return the topics
	 */
	public List<TopicInfo> getTopics() {
		return topics;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @param topics the topics to set
	 */
	public void setTopics(List<TopicInfo> topics) {
		this.topics = topics;
	}
	@Override
	public String toString() {
		return "Consumer [address=" + address + ", topics=" + topics + "]";
	}
	
}
