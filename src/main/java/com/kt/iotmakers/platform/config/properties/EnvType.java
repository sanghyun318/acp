package com.kt.iotmakers.platform.config.properties;

public enum EnvType {

	LOCAL("local"),
	DEV("dev"),
	PROD("prod");

	private final String value;

	private EnvType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	public boolean isLocal(){
		return this.value.equals(LOCAL.value)?true:false;
	}
	 public  static EnvType getEnvTypeFromString(String inputCode){
		 for(EnvType type : EnvType.values()){
			 if(type.value.equals(inputCode)) return type;
		 }
		 return null;
	 }
}
