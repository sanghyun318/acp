package com.kt.iotmakers.platform.config.properties;

public class KafkaInfo {
	
	Consumer consumer;
	Producer producer;
	/**
	 * @return the consumer
	 */
	public Consumer getConsumer() {
		return consumer;
	}
	/**
	 * @return the producer
	 */
	public Producer getProducer() {
		return producer;
	}
	/**
	 * @param consumer the consumer to set
	 */
	public void setConsumer(Consumer consumer) {
		this.consumer = consumer;
	}
	/**
	 * @param producer the producer to set
	 */
	public void setProducer(Producer producer) {
		this.producer = producer;
	}
	@Override
	public String toString() {
		return "KafkaInfo [consumer=" + consumer + ", producer=" + producer + "]";
	}
	
}
