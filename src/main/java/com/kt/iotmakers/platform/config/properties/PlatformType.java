package com.kt.iotmakers.platform.config.properties;

public enum PlatformType {

	VM("vm"),
	K8S("k8s");

	private final String value;

	private PlatformType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	
	 public  static PlatformType getEnvTypeFromString(String inputCode){
		 for(PlatformType type : PlatformType.values()){
			 if(type.value.equals(inputCode)) return type;
		 }
		 return null;
	 }
}
