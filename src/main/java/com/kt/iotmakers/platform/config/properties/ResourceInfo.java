package com.kt.iotmakers.platform.config.properties;

import java.util.ArrayList;
import java.util.List;

public class ResourceInfo {

	List<String> urls; 
	String requestUrl;
	String name;
	String pattern;
	String parsing;
	ResourceValidType type;
	/**
	 * @return the urls
	 */
	public List<String> getUrls() {
		return urls;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @return the pattern
	 */
	public String getPattern() {
		return pattern;
	}
	/**
	 * @param pattern the pattern to set
	 */
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}
	/**
	 * @return the type
	 */
	public ResourceValidType getType() {
		return type;
	}
	/**
	 * @param urls the urls to set
	 */
	public void setUrls(List<String> urls) {
		this.urls = urls;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(ResourceValidType type) {
		this.type = type;
	}
	/**
	 * @return the requestUrl
	 */
	public String getRequestUrl() {
		return requestUrl;
	}
	/**
	 * @param requestUrl the requestUrl to set
	 */
	public void setRequestUrl(String requestUrl) {
		this.requestUrl = requestUrl;
	}
	/**
	 * @return the parsing
	 */
	public String getParsing() {
		return parsing;
	}
	/**
	 * @param parsing the parsing to set
	 */
	public void setParsing(String parsing) {
		this.parsing = parsing;
	}
	@Override
	public String toString() {
		return "ResourceInfo [urls=" + urls + ", requestUrl=" + requestUrl + ", name=" + name + ", pattern=" + pattern
				+ ", parsing=" + parsing + ", type=" + type + "]";
	}
}
