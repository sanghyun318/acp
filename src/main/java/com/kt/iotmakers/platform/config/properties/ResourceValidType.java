package com.kt.iotmakers.platform.config.properties;

public enum ResourceValidType {

	DB("00"),
	API("01");
	
   private String value;
   
   private ResourceValidType (String value) {
           this.value = value;
   }

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
}
