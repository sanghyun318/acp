/*
 * GiGA IoT Platform version 2.0
 *
 *  Copyright ⓒ 2015 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package com.kt.iotmakers.platform.master;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.filter.ShallowEtagHeaderFilter;

import com.kt.iotmakers.platform.Const;
import com.kt.iotmakers.platform.api.VO.Contract.Contract;
import com.kt.iotmakers.platform.common.Constant.OperationType;
import com.kt.iotmakers.platform.common.base.messaging.service.MessagingOperation;

@Configuration
@ComponentScan(basePackages = {Const.PACKAGE})
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class })
@EnableConfigurationProperties
public class AcpApplication {
//extends SpringBootServletInitializer implements WebApplicationInitializer {


	private static final Logger logger = LoggerFactory.getLogger(AcpApplication.class);
	
//	@Bean
//	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
//		PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
//		return configurer;
//	}
////
//	@Override
//    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//        return application.sources(AcpApplication.class);
//    }

//	@Override
//	public void onStartup(ServletContext servletContext) throws ServletException {
//		
//		AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
////		servletContext.addListener(new ContextLoaderListener(context));
////		context.setConfigLocation(Const.PACKAGE);
////		context.setServletContext(servletContext);
//
////		Dynamic servlet = servletContext.addServlet("DispatcherServlet", new DispatcherServlet(context));
////		servlet.setLoadOnStartup(1);
////		servlet.addMapping("/*");
////		servlet.setMultipartConfig(new MultipartConfigElement("", 1024*1024*50, 1024*1024*100, 1024*1024*10));
////		servlet.setAsyncSupported(true);
////
////		// CharacterSet Encoding
////		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
////        characterEncodingFilter.setEncoding("UTF-8");
////        characterEncodingFilter.setForceEncoding(true);
////
////        FilterRegistration.Dynamic charEncRegi = servletContext.addFilter("characterEncodingFilter", characterEncodingFilter);
////        EnumSet<DispatcherType> dispatcherTypes = EnumSet.of(DispatcherType.REQUEST, DispatcherType.FORWARD);
////        charEncRegi.addMappingForUrlPatterns(dispatcherTypes, true, "/*");
////        charEncRegi.setAsyncSupported(true);
////
////        FilterRegistration.Dynamic loggerFilter = servletContext.addFilter("loggerFilter", new LoggerFilter());
////        EnumSet<DispatcherType> dispatcherTypesForLoggerFilter = EnumSet.of(DispatcherType.REQUEST, DispatcherType.ASYNC, DispatcherType.FORWARD);
////        loggerFilter.addMappingForUrlPatterns(dispatcherTypesForLoggerFilter, true, "/*");
////        loggerFilter.setAsyncSupported(true);
////
////        FilterRegistration.Dynamic shallowEtagHeaderFilter = servletContext.addFilter("shallowEtagHeaderFilter", new ShallowEtagHeaderFilter());
////        EnumSet<DispatcherType> shallowEtagHeaderFilterTypes = EnumSet.of(DispatcherType.REQUEST, DispatcherType.ASYNC, DispatcherType.FORWARD);
////        shallowEtagHeaderFilter.addMappingForUrlPatterns(shallowEtagHeaderFilterTypes, true, "/*");
////        shallowEtagHeaderFilter.setAsyncSupported(true);
//
//      
//		super.onStartup(servletContext);
//	}
	//add for Etag
	@Bean
	public ShallowEtagHeaderFilter shallowEtagHeaderFilter() {
		return new ShallowEtagHeaderFilter();
	}

	public static void main(String[] args) {
		SpringApplication.run(AcpApplication.class, args);
	}
	
	@RestController
	class TestController {
		
		final String  targetTopic = "MASTER.TARGET";
		final String  deviceTopic = "MASTER.DEVICE";
				
		@Autowired
		MessagingOperation<Contract> messagingOperations;

		@PostMapping("/target")
		private void makeTarget(@RequestBody Contract target, @RequestParam OperationType operationType, HttpServletResponse response) {


			logger.info("in produceTarget : {}, {}", target, operationType);
			sendMasterChangedEvent(operationType, target);
			response.setStatus(200);

		}

		private void sendMasterChangedEvent(OperationType operationType, Contract target) {
			logger.info("in sendMasterChangedEvent : {}, {}", operationType, target);

			messagingOperations.sendMasterChangedMessage(targetTopic, "v2", operationType, target, 10);
		}
	}
}
